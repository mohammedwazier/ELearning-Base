
exports.up = function(knex) {
    return knex.schema.createTable('bank', function(table){
        table.bigIncrements();
        table.string('bank_id').nullable();
        table.string('bank_name').nullable();
        table.string('bank_kode').nullable();
        table.string('bank_nama').nullable();
        table.string('bank_rekening').nullable();
        table.string('bank_image').nullable();
        table.string('bank_keterangan').nullable()
        table.timestamp('bank_created_at').defaultTo(knex.fn.now());
        table.timestamp('bank_updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('bank');
};
