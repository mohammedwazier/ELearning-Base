
exports.up = function(knex) {
	return knex.schema.createTable('cashflow', function(table){
		table.bigIncrements('cf_id').primary();

		table.string('cf_keterangan');
		table.string('cf_tipe');
		table.float('cf_kredit');
		table.float('cf_debit');
		table.string('cf_nominal');
		table.string('cf_refid');
		table.string('cf_internal_acc');
		table.string('cf_profile_id');
		table.timestamp('cf_created_at');
		table.string('cf_mode');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('cashflow');
};
