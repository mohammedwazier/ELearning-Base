
exports.up = function(knex) {
    return knex.schema.createTable('otp_list',function(table){
        table.bigIncrements();
        table.string('otp_nohp');
        table.string('otp_kode');
        table.integer('otp_status').defaultTo(0);
        table.timestamp('otp_created_at').defaultTo(knex.fn.now());
        table.timestamp('otp_updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('otp_list')
};
