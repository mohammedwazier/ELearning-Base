
exports.up = function(knex) {
	return knex.schema.createTable('userguide', function(table){
		table.bigIncrements();
		table.string('ug_id').notNull().unique();
		table.string('ug_name');
		table.timestamp('ug_created_at').defaultTo(knex.fn.now());
		table.timestamp('ug_updated_at').nullable();
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('userguide');
};
