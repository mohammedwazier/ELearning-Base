
exports.up = function(knex) {
	return knex.schema.createTable('account', function(table){
		table.bigIncrements('acc_id').primary();

		table.string('acc_nama').notNull();
		table.string('acc_tipe').notNull();
		table.string('acc_noakun').notNull();
		table.bigInteger('acc_saldo').defaultTo(0);
		table.timestamp('acc_created_at').defaultTo(knex.fn.now());
		table.timestamp('acc_updated_at').defaultTo(knex.fn.now());
		table.string('acc_isactive', 1).defaultTo('1');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('account');
};
