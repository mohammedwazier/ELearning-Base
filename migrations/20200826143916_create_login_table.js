
exports.up = function(knex) {
    return knex.schema.createTable('login',function(table){
        table.bigIncrements();
        table.string('log_profile_id');
        table.string('log_token');
        table.string('log_type');
        table.timestamp('log_created_at').defaultTo(knex.fn.now());
        table.integer('log_status').defaultTo(1);
        table.string('log_ip');
        table.string('log_data');
        table.string('log_lat');
        table.string('log_ing');
        table.string('log_firebase_tok');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('login')
};
