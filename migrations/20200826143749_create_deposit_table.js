
exports.up = function(knex) {
    return knex.schema.createTable('deposit', function(table){
        table.bigIncrements();
        table.string('dep_id').nullable();
        table.bigInteger('dep_total').defaultTo(0);
        table.bigInteger('dep_kode_unik').defaultTo(0);
        table.string('dep_id_profile').nullable();
        table.timestamp('dep_expired').nullable();
        table.integer('dep_status').defaultTo(0);
        table.timestamp('dep_created_at').defaultTo(knex.fn.now());
        table.timestamp('dep_updated_at').defaultTo(knex.fn.now());
        table.string('dep_admin_id').nullable();
        table.bigInteger('dep_nominal').defaultTo(0);
        table.string('dep_image').nullable();
        table.bigInteger('dep_bank_kode').defaultTo(0);
        table.string('dep_refid');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('deposit');
};
