
exports.up = function(knex) {
    return knex.schema.createTable('event', function(table){
        table.bigIncrements();
        table.string('event_id');
        table.string('event_kode');
        table.string('event_nama');
        table.string('event_cover');
        table.string('event_keterangan');
        table.string('event_type');
        table.timestamp('event_startdate');
        table.timestamp('event_enddate');
        table.time('event_starthour');
        table.time('event_endhour');
        table.integer('event_method');
        table.integer('event_value');
        table.timestamp('event_created').defaultTo(knex.fn.now());
        table.timestamp('event_updated').defaultTo(knex.fn.now());
        table.integer('event_isactive').defaultTo(1);
        table.string('event_admin');
        table.integer('event_max').defaultTo(0);
        table.integer('event_count').defaultTo(0);
        table.string('event_paymethod');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('event');
};
