
exports.up = function(knex) {
    return knex.schema.createTable('produk_group', function(table){
        table.bigIncrements();

        table.string('id_group');
        table.string('group_nama');
        table.string('is_active', 2);
        table.string('gambar_group');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('produk_group');
};
