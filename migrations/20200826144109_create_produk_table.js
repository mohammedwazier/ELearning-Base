
exports.up = function(knex) {
    return knex.schema.createTable('produk', function(table){
        table.bigIncrements();

        table.string('produk_namaProduk');
        table.string('produk_id_group');
        table.integer('produk_harga');
        table.string('produk_kodeProduk');
        table.string('produk_keterangan');
        table.timestamp('produk_created_at').defaultTo(knex.fn.now());
        table.timestamp('produk_updated_at').defaultTo(knex.fn.now());
        table.string('produk_cover');
        table.string('produk_id');
        table.string('produk_link');
        table.string('produk_id_profile');
        table.string('produk_certificate');
        table.integer('produk_is_active');
        table.string('produk_pemateri_id');
        table.integer('produk_viewed').defaultTo(0);
        table.timestamp('produk_start');
        table.timestamp('produk_end');
        table.integer('produk_buy');
        table.integer('produk_max_buy');
        table.string('produk_thumb');
        table.string('produk_announce');
        table.time('produk_startjam');
        table.time('produk_endjam');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('produk');
};
