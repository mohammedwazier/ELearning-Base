
exports.up = function(knex) {
    return knex.schema.createTable('evoucher', function(table){
        table.bigIncrements();
        table.string('evc_nama');
        table.string('evc_jenis');
        table.bigInteger('evc_value');
        table.string('evc_keterangan');
        table.timestamp('evc_expired');
        table.integer('evc_kuota');
        table.timestamp('evc_created_at');
        table.timestamp('evc_updated_at');
        table.integer('evc_status').defaultTo(1);
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('evoucher');
};
