
exports.up = function(knex) {
	return knex.schema.createTable('code_voucher', function(table){
		table.bigIncrements('cvc_id').primary();
		table.string('cvc_kode');
		table.string('cvc_id_kode');
		table.string('cvc_used', 1).defaultTo('0');
		table.string('cvc_user_id');
		table.timestamp('cvc_created_at').defaultTo(knex.fn.now());
		table.timestamp('cvc_updated_at');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('code_voucher');
};
