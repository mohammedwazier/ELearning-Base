
exports.up = function(knex) {
    return knex.schema.createTable('list_mutasi', function(table) {
        table.bigIncrements();
        table.string('service_name');
        table.string('service_code');
        table.string('account_number');
        table.string('account_name');
        table.integer('service_id').unique();
        table.integer('unix_timestamp');
        table.string('type');
        table.string('amount');
        table.string('description');
        table.string('balance');
        table.timestamp('created').defaultTo(knex.fn.now());
        table.string('refid_trx');
        table.integer('mutasi_status').defaultTo(0);
        table.string('action');
        table.string('timezone');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('list_mutasi');
};
