
exports.up = function(knex) {
    return knex.schema.createTable('outbox',function(table){
        table.bigIncrements('obx_id').primary();
        table.string('obx_refid');
        table.string('obx_id_profile');
        table.string('obx_interface');
        table.string('obx_tipe');
        table.string('obx_status');
        table.string('obx_format_msg');
        table.string('obx_keterangan');
        table.timestamp('obx_created_at').defaultTo(knex.fn.now());
        table.timestamp('obx_updated_at').defaultTo(knex.fn.now());
        table.string('obx_isactive');
        table.string('obx_raw_data');
    })
};

exports.down = function(knex) {
  
};
