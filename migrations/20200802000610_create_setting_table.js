// let MainController = require('@Controllers/MainController');
// MainController = new MainController;

exports.up = function(knex) {
	// const driver = MainController.getDatabaseDriver(knex);
	return knex.schema.hasTable('setting')
	.then((exist) => {
		if(!exist){
			return knex.schema.createTable('setting', function(table){
				table.bigIncrements('st_id').primary();
				table.string('st_keterangan');
				table.string('st_kode', 20).notNull();
				// if(driver === 'pg'){
					/*Postgresql*/
					table.specificType('st_value', 'character varying').notNull();
				// }else{
				// 	/*Mysql*/
				// 	table.text('st_value', 'longtext').notNull();
				// }

				table.timestamp('st_created_at').defaultTo(knex.fn.now());
				table.timestamp('st_updated_at').nullable();
				table.string('st_id_admin').notNull();
				table.string('st_isactive', 1).defaultTo('1');
				table.string('st_typevalue').nullable();
			})
		}
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('setting');
};
