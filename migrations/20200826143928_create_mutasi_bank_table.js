
exports.up = function(knex) {
    return knex.schema.createTable('mutasi_bank',function(table){
        table.bigIncrements();
        table.string('mutasi_raw').notNullable();
        table.timestamp('mutasi_created').defaultTo(knex.fn.now());
        table.timestamp('mutasi_updated_at').defaultTo(knex.fn.now());
        table.integer('mutasi_status').defaultTo(0);
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('mutasi_bank');
};
