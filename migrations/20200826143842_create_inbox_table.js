
exports.up = function(knex) {
    return knex.schema.createTable('inbox', function(table){
        table.integer('inx_id').primary();
        table.string('ibx_refid');
        table.string('ibx_id_profile');
        table.string('ibx_interface');
        table.string('ibx_tipe');
        table.string('ibx_status').defaultTo('Q');
        table.string('ibx_format_msg');
        table.string('ibx_keterangan');
        table.timestamp('ibx_created_at');
        table.timestamp('ibx_updated_at');
        table.string('ibx_isactive', 1);
        table.string('ibx_raw_data');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('inbox');
};
