
exports.up = function(knex) {
    return knex.schema.createTable('placement', function(table){
        table.bigIncrements();
        table.string('placement_prod');
        table.string('placement_data');
        table.timestamp('placement_created_at').defaultTo(knex.fn.now());
        table.timestamp('placement_updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('placement');
};
