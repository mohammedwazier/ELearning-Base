exports.up = function (knex) {
    return knex.schema.createTable('access_api', function (table) {
        table.increments('id').primary();

        table.string('acc_ip').nullable();
        table.string('acc_url').nullable();
        table.timestamp('acc_created').defaultTo(knex.fn.now());
        table.string('acc_uid').nullable();
        table.string('acc_node').nullable();
        table.specificType('acc_token', 'character varying').nullable();
        table.specificType('acc_data', 'character varying').nullable();
        table.string('acc_memory').nullable().comment('Memory used for process');
        table.index(['acc_ip', 'acc_url', 'acc_node', 'acc_token'], 'access_api_index');
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('access_api');
};
