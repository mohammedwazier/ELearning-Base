module.exports = {
  apps : [
  {
      name: '[PROD] Main Server',
      script: 'index.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] Topup Deposit',
      script: './src/Jobs/TopupDeposit/TopupDeposit.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] Payment Certificate',
      script: './src/Jobs/BuyCertificate/BuyCertificate.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] Payment Ebook',
      script: './src/Jobs/BuyEbook/BuyEbook.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] Payment EPresentasi',
      script: './src/Jobs/BuyPresentasi/BuyPresentasi.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] Deposit Expired',
      script: './src/Jobs/DepositExpire/DepositExpire.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] OTP Expired',
      script: './src/Jobs/OTPExpire/OTPExpire.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] Payment Profisiensi',
      script: './src/Jobs/BuyProfisiensi/BuyProfisiensi.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] Cron Mutasi OVO',
      script: './src/Jobs/MutasiCron/MutasiOVO.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] Insert Mutasi to Inbox',
      script: './src/Jobs/TopupDeposit/InsertTopup.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[PROD] Event Cron',
      script: './src/Jobs/EventCron/EventCron.js',
      args: '',
      instances: 1,
      exec_mode: 'fork',
      log_date_format: 'YYYMMDD HH',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    }
  ],
  deploy : {
    production : {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
