// Update with your config settings.
require('module-alias/register');
const knexFile = require('./src/Connection/knexFile.js');

module.exports = knexFile[process.env.ENV];
