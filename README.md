# Ekoji-Base-Backend
Base Core (Backend and Cron)
-----------------------

Testing edit readme from VIM Text editor


Notes : 

    Password Encryption Step

    1. Password
    2. Convert to Base64
    3. Encode t UTF8
    4. Encrypt to SHA1 with Cipher/Salt = 
    5. Done Password
 	

ENV Primary :

    ENV=
    NODE_ENV=
    PORT=
    SUB_PORT= #SPLITTING File with Comma
    USERDB=
    PASSDB=
    HOSTDB=
    PORTDB=
    DATABASE=
    DIALECTDB=
    ACCOUNTSID=
    AUTHTOKEN=
    TWLNUMBER=
    CIPHERID=
    GEOLOCATION=
    FIREBASE=
    YOUTUBEKEY=
    BANKAPIKEY=
    BANKSIGNATURE=
    APISLACK=

## How to Deploy in server (How To Use)
### Step by Step
```
    'cp .env.example .env'
    'npm install' or 'yarn install'
    'fill .env files with data'
    'pm2 start index.js -i max'
```

## Build Status
[![Build Status](https://travis-ci.org/mohammedwazier/ELearning-Base.svg?branch=master)](https://travis-ci.org/mohammedwazier/ELearning-Base)

![Node.js CI](https://github.com/mohammedwazier/ELearning-Base/workflows/Node.js%20CI/badge.svg?branch=master)

## Developer
[![Mohammedwazier](https://avatars.githubusercontent.com/u/5354070?s=130)](https://mohammedwazier.github.io/) |
---|
[Masihkasar](https://github.com/mohammedwazier) |

> Muhammad Waziruddin Akbar © 2020