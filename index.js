require('module-alias/register');

const express = require('express');
// const { Request, Response } = require('express');
const Cors = require('cors');
const Compression = require('compression');
const { urlencoded, json } = require('body-parser');
const helmet = require('helmet');
const { createServer } = require('http');
const Cookie = require('cookie-parser');
const timeout = require('connect-timeout');

const LoggingAccess = require('./src/Middleware/LoggingAccess');

const { PORT, NAME, VERSION, NODE_ENV } = require('@Config/Config');
const app = express();
const server = createServer(app);


app.use(timeout('60s'))
app.use(helmet());
app.use(Cors());
app.use(Cookie());
app.use(Compression())
app.use(urlencoded({extended: false}))
app.use(json())
app.use(LoggingAccess) /*Logging All Access*/

app.disable('x-powered-by');

app.use('/api', require('@Service/index'));
app.get('/', (req, res) => {
    return res.send('Hello World');
})

app.use('/files', express.static('./src/Source'));
app.use((req, res, next) => {
	res.status(404).send({code: 404, error: 'Route Not Found'});
})

app.listen(PORT, () => {
    console.log(`Service Server ${NAME}\nversion ${VERSION}\nrunning on localhost:${PORT} [${NODE_ENV}]\n`);
    console.log(`API Endpoint /api/v${VERSION.split('.')[0]}/{ nama modul }`);
})

// Handle uncaughtError
process.on("uncaughtException", e => {
    console.log('uncaughtException',e.stack);
});
// Handle Error
process.on("unhandledRejection", e => {
    console.log('unhandledRejection', e.stack);
});

process.on('message', function (msg) {
    if (msg == 'shutdown') {
        console.log('asdasd, shutdown');
        /* server.end().then(() => {
            process.exit();
        }).catch((err) => {
            console.error(err);
        }); */
    }
});


module.exports = app;