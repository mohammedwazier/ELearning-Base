const { DATABASE } = require('../Config/Config');
const { dialect, uname, passwd, host, port, db } = DATABASE;


const defaultConfig = {
    client: dialect,
    connection: {
        host: host,
        user: uname,
        password: passwd,
        database: db,
    },
}

const knexConfig = {
    local: {
        ...defaultConfig,
        pool: { min: 0, max: 7 }
    },

    development: {
        useNullAsDefault: true,
        ...defaultConfig,
        pool: { min: 0, max: 7 }
    },

    production: {
        client: dialect,
        connection: {
            host: `localhost`,
            user: uname,
            password: passwd,
            database: db
        },
        pool: { min: 0, max: 7 }
    }
};


module.exports = knexConfig;