// import knex, { Config } from 'knex';
const knex = require('knex');

const knexConfig = require('./knexFile.js');

const { NODE_ENV } = require('../Config/Config');

let KNEX = knex(knexConfig[NODE_ENV]);

module.exports = KNEX;