const { STRUCTURE, URLIMAGE } = require('@Config/Config');
const database = require('@Model/index');
const MainController = require('@Controllers/MainController');
const SendErrorController = require('@Controllers/SendErrorController');

class EventController extends MainController {
	structure;
	constructor(){
		super();
		this.structure = STRUCTURE;
	}

	checkingEvent = (type, date) => {
		return new Promise(async resolve => {
            try{
    			let newDate;
    			if(date){
    				date = this.createDate(0);
    			}
    			newDate = this.convertDate(date);
    			let data = await database.event.connection.raw(`SELECT * FROM event WHERE event_type = '${type}' AND '${newDate}' BETWEEN event_startdate AND event_enddate AND event_isactive = 1 LIMIT 1`)
    			if(data.rows.length > 0){
    				resolve({state: true, data: data.rows[0]})
    			}else{
    				resolve({state: false, data: {}})
    			}
            }catch(err){
                SendErrorController.message(`Error on EventController\n${err.stack}`)
                resolve({state: false, data: {}})
            }
		})
	}

	createEventInbox = (profile_id, eventid, eventkode, value, namaEvent) => {
		return new Promise(async resolve => {
            try{
    			let refid = `EVENT${this.generateID()}`;

    	        let format_msg = `EVENT_REDEEM.${eventkode}.${eventid}.${value}.${profile_id}.${refid}`;
    	        const insertInbox = {
    	            ibx_refid: refid,
    	            ibx_id_profile: profile_id,
    	            ibx_interface: 'H',
    	            ibx_tipe: 'EVENTREDEEM',
    	            ibx_status: 'Q',
    	            ibx_format_msg: format_msg,
    	            ibx_keterangan: `Berhasil input ke inbox pada ${this.createDate(0)}`,
    	            ibx_raw_data: JSON.stringify({profile_id, eventid, eventkode, value, namaEvent})
    	        }
            	let harga = Number(value);
    	        let trxID = this.generateID();
    	        let trxINV = this.createInvoice('EVENT');
    	        let transaksiData = {
    	            trx_id: trxID,
    	            trx_keterangan: 'Transaksi sedang dalam proses',
    	            trx_tipe: 'EVENT',
    	            trx_id_tipe: 'EVENTREDEEM',
    	            trx_harga: harga,
    	            trx_fee: 0,
    	            trx_total_harga: harga,
    	            trx_saldo_before: 0,
    	            trx_saldo_after: harga,
    	            trx_status: 'Q',
    	            trx_id_profile: profile_id,
    	            trx_code_voucher: '',
    	            trx_invoice: trxINV,
    	            trx_refid: refid,
    	            trx_produk_id: eventid,
    	            trx_judul: namaEvent
    	        }
    	        let transaksi = await database.transaksi.insert(transaksiData);
    	        let inbox = await database.inbox.insertOne(insertInbox);

    	        resolve({
    	        	transaksi: transaksi,
    	        	inbox: inbox
    	        })
            }catch(err){
                SendErrorController.message(`Error on EventController\n${err.stack}`)
                resolve({
                    state: false
                })
            }
		})
	}

	getAllEvent = (fields, body) => {
		let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let limit = 10;

                    let start = (body.page - 1) * limit;
                    let count = await database.event.connection.raw(`SELECT COUNT(*) FROM event`);
                    count = count.rows[0].count;
                    // let query = `SELECT * FROM event LIMIT ${limit} OFFSET ${start}`;
                    let query = `
                    SELECT
                    a.*,
                    CASE WHEN a.event_cover IS NOT NULL
                    THEN CONCAT('${URLIMAGE}', a.event_cover)
                    ELSE NULL
                    END as event_coverlink
                    FROM
                    event a
                    ORDER BY event_created DESC
                    LIMIT ${limit} OFFSET ${start}
                    `
                    let data = await database.event.connection.raw(query);
                    let output = {
                        list: data.rows,
                        start: start,
                        total: count,
                        per_page: limit,
                        current_page: body.page
                    }
                	response.data = output;
                    response.message = `Success get Event Data`;
                    response.code = 100
                    response.state = true
                    resolve(response);
                }else{
                	response.data = [];
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on EventController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                resolve(err)
            }
        })
	}

    searchEvent = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let limit = 10;

                    let start = (body.page - 1) * limit;
                    let count = await database.event.connection.raw(`SELECT COUNT(*) FROM event`);
                    count = count.rows[0].count;
                    // let query = `SELECT * FROM event LIMIT ${limit} OFFSET ${start}`;
                    let query = `
                    SELECT
                    a.*,
                    CASE WHEN a.event_cover IS NOT NULL
                    THEN CONCAT('${URLIMAGE}', a.event_cover)
                    ELSE NULL
                    END as event_coverlink
                    FROM
                    event a
                    WHERE 
                    LOWER(a.event_kode) LIKE LOWER('%${body.search}%')
                    OR
                    LOWER(a.event_nama) LIKE LOWER('%${body.search}%')
                    OR
                    CAST(a.event_value as varchar) LIKE LOWER('%${body.search}%')
                    OR
                    LOWER(event_type) LIKE LOWER('%${body.search}%')
                    ORDER BY event_created DESC
                    LIMIT ${limit} OFFSET ${start}
                    `
                    let data = await database.event.connection.raw(query);
                    let output = {
                        list: data.rows,
                        start: start,
                        total: count,
                        per_page: limit,
                        current_page: body.page
                    }
                    response.data = output;
                    response.message = `Success Search, Event Found ${data.rows.length}`
                    response.code = 100
                    response.state = true
                    resolve(response);
                }else{
                    response.data = [];
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on EventController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                resolve(err)
            }
        });
    }

	createEvent = (fields, body) => {
		let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                	let data = {
                		event_id: this.generateID(),
                		event_kode: this.makeid(10),
                		event_nama: body.name,
                		event_cover: body.cover,
                		event_keterangan: body.description,
                		event_type: body.event_type,
                		event_startdate: body.startdate,
                		event_enddate: body.enddate,
                		event_starthour: body.starthour || '00:00:00',
                		event_endhour: body.endhout || '23:59:00',
                		event_method: body.method,
                		event_value: body.nilai,
                		event_admin: body.id,
                		event_max: body.used,
                		event_paymethod: body.pay_method
                	}
                	let insert = await database.event.insertOne(data);
                	console.log(insert)
                	response.data = body;
                    response.message = `Success insert event data`;
                    response.code = 100
                    response.state = true
                    resolve(response);
                }else{
                	response.data = [];
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
            	SendErrorController.message(`Error on EventController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                resolve(err)
            }
        })
	}

    single = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    // let data = await database.event.single({event_id: body.event_id});
                    let data = await database.event.connection.raw(`
                        SELECT
                        a.*,
                        CASE WHEN a.event_cover IS NOT NULL
                        THEN CONCAT('${URLIMAGE}', a.event_cover)
                        ELSE NULL
                        END as event_coverlink
                        FROM
                        event a
                        WHERE event_id = '${body.event_id}'
                        `)
                    response.data = data.rows[data.rows.length - 1];
                    response.message = `Success get single event`;
                    response.code = 100
                    response.state = true
                    resolve(response);
                }else{
                    response.data = [];
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on EventController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                resolve(err)
            }
        })
    }

    softDelete = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let data = await database.event.updateOne({event_id: body.eventid}, {event_isactive: 2});
                    if(data.state){
                        response.data = body;
                        response.message = `Success SoftDelete Event`;
                        response.code = 100
                        response.state = true
                        resolve(response);

                    }else{
                        response.data = body;
                        response.message = `Failed SoftDelete Event`;
                        response.code = 103
                        response.state = false
                        resolve(response);
                    }
                }else{
                    response.data = [];
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on EventController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                resolve(err)
            }
        })
    }

    updateEvent = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                   let data = {
                        event_id: body.eventid,
                        event_nama: body.name,
                        event_keterangan: body.description,
                        event_type: body.event_type,
                        event_startdate: body.startdate,
                        event_enddate: body.enddate,
                        event_starthour: body.starthour,
                        event_endhour: body.endhout,
                        event_method: body.method,
                        event_value: body.nilai,
                        event_admin: body.id,
                        event_updated: this.createDate(0),
                        event_max: body.used,
                        event_paymethod: body.pay_method
                    }
                    if(body.cover){
                        data = {
                            ...data,
                            cover: body.cover
                        }
                    }
                    let updatedData = await database.event.updateOne({event_id: body.eventid}, data);
                    if(updatedData.state){
                        response.data = body;
                        response.message = `Success Update Event ${data.event_nama}`;
                        response.code = 100
                        response.state = true
                        resolve(response);

                    }else{
                        response.data = body;
                        response.message = `Failed Update Event`;
                        response.code = 103
                        response.state = false
                        resolve(response);
                    }
                }else{
                    response.data = [];
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on EventController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                resolve(err)
            }
        })
    }
}

module.exports = new EventController;