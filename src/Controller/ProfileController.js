const database = require('@Model/index');
const MainController = require('@Controllers/MainController');
const { STRUCTURE, URLIMAGE, WHATSAPP } = require('@Config/Config');
const SendErrorController = require('@Controllers/SendErrorController');
const { kodeOtpSelect } = require('@Query/QueryModel');

const fs = require('fs-extra');
const path = require('path')

const kota = require('@Util/KotaIndonesia.json');

const { accountSid, authToken, number } = WHATSAPP;

const client = require('twilio')(accountSid, authToken);
const MessagingResponse = require('twilio').twiml.MessagingResponse;

const EmailController = require('@Controllers/EmailController')

class ProfileController extends MainController{
    structure = STRUCTURE;
    constructor(){
        super();
    }

    getDetailProfile = (fields, body) => {
        let response = this.structure;
        return new Promise(async resolve => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let profile = await database.profile.allSelect({prl_profile_id: body.id, prl_isactive: 1});
                    if(profile.length > 0){
                        profile = profile[profile.length - 1];
                        // console.log(profile);
                        const data = {
                            id: profile.prl_profile_id,
                            nama: profile.prl_nama,
                            tanggal_lahir: profile.prl_tanggal_lahir,
                            tempat_lahir: profile.prl_tempat_lahir,
                            nohp: profile.prl_nohp,
                            gender: profile.prl_gender,
                            username: profile.prl_username,
                            email: profile.prl_email,
                            gelar: profile.prl_gelar,
                            gelar_profesi: profile.prl_gelar_profesi,
                            institusi: profile.prl_institusi,
                            saldo: profile.prl_saldo_nexus,
                            photo: profile.prl_photo ? `${URLIMAGE}${profile.prl_photo}` : `${URLIMAGE}img_default.PNG`
                        }
                        response.data = data;
                        response.message = "Data Found";
                        response.code = 100;
                        response.state = true;
                        resolve(response);
                    }else{
                        response.data = {};
                        response.message = "Profile not Found";
                        response.code = 103;
                        response.state = false
                        throw response;
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    throw response;
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        })
    }

    updateProfile = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    // Check id and Password
                    let id = this.decipherToken(body.token);
                    if(id.id !== body.id){
                        response.data = {};
                        response.code = 106;
                        response.state = false;
                        response.message = 'ID Not Valid';
                    }
                    let where = {
                        prl_profile_id: body.id,
                        prl_isactive: 1
                        // prl_password: this.createPassword(body.password)
                    }
                    let profile = await database.profile.allSelect(where);
                    // console.log
                    if(profile.length > 0){
                        profile = profile[0];
                        let update = body;
                        delete update.password;
                        if(newBody.includes('newPassword')){
                            update.password = this.createPassword(body.newPassword);
                        }
                        delete update.id;
                        delete update.newPassword;
                        delete update.token;
                        let moveToDbObj = Object.entries(update);
                        let objUpdate = {};
                        for(let idx = 0; idx < moveToDbObj.length; idx++){
                            if(moveToDbObj[idx][1]){
                                objUpdate[`prl_${moveToDbObj[idx][0]}`] = moveToDbObj[idx][1];
                            }
                        }
                        objUpdate.prl_updated_at = this.createDate();
                        let updated = await database.profile.updateOne({prl_profile_id: profile.prl_profile_id}, objUpdate)
                        if(updated.state){
                            delete update.password;
                            if(moveToDbObj.includes('photo')){
                                const uploadPath = path.join(__dirname, '../Source/');
                                update.photo = URLIMAGE+update.photo;
                                update.size = this.getFileSize(path.join(uploadPath, update.photo))
                            }
                            // console.log(update)
                            response.data = update;
                            response.message = "Success Update Profile";
                            response.code = 100;
                            response.state = true
                            resolve(response);
                        }else{
                            response.data = {};
                            response.message = "Failed to update profile";
                            response.code = 104;
                            response.state = false
                            throw response;
                        }
                    }else{
                        response.data = {};
                        response.message = "Profile not Found or Password or username are failed";
                        response.code = 103;
                        response.state = false
                        throw response;
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    throw response;
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                response.code = 503;
                response.state = false
                response.message = err.stack;
                resolve(response);
            }
        })
    }

    checkUsername = (fields, body) => {
        let response = this.structure;
        return new Promise(async resolve => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let profile = await database.profile.connection.raw(`SELECT * FROM profile WHERE prl_username = '${body.username}' AND prl_isactive = 1`);
                    if(profile.rows.length === 0){
                        response.data = body;
                        response.message = "Username not found";
                        response.code = 100;
                        response.state = true
                        resolve(response)
                    }else{
                        response.data = {};
                        response.message = "Username was Exist";
                        response.code = 103;
                        response.state = false
                        throw response;
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    throw response;
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    getKota = () => {
        let response = this.structure;
        try{
            const obj = Object.keys(kota);
            let newKota = new Array();
            for(let idx = 0; idx < obj.length; idx++){
                let data = kota[obj[idx]];
                let sub1 = Object.keys(data);
                for(let idc = 0; idc < data[sub1[0]].length; idc++){
                    let subData = data[sub1[0]][idc];
                    let tipe = typeof subData
                    if(tipe === 'string'){
                        newKota.push(subData)
                    }else{
                        if(Array.isArray(subData)){
                            // console.log('Array', subData)
                        }else{
                            let sub2 = Object.keys(subData);
                            for(let idv = 0; idv < sub2.length; idv++){
                                if(Array.isArray(subData[sub2[idv]])) newKota.concat(subData[sub2[idv]]);
                            }
                        }
                    }
                }
            }
            response.data = newKota;
            response.state= 100;
            response.state = true;
            response.message = `Success get city in indonesia`;
            return response;
        }catch(err){
            console.error(err)
            SendErrorController.message(`Error on ProfileController\n${err.stack}`)
            response.data = {};
            response.state= 102;
            response.state = false;
            response.message = `Error to get File`;
            return response;
        }
    }

    updatePhoto = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let updated = await database.profile.updateOne({prl_profile_id: body.id}, {prl_photo: body.image});
                    if(updated.state){
                        response.data = {
                            id: body.id,
                            image: body.image,
                            linkImage: URLIMAGE + body.image
                        };
                        response.message = "Success Update Photo Profile";
                        response.code = 100;
                        response.state = true
                        resolve(response);
                    }else{
                        response.data = {};
                        response.message = "Failed to update Photo Profile, profile not found";
                        response.code = 104;
                        response.state = false
                        throw response;
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    throw response;
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    getProfilePicture = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let data = await database.profile.allSelect({prl_profile_id: body.id, prl_isactive: 1});
                    // console.log(data)
                    if(data.length > 0){
                        data = data[0];
                        response.data = {
                            image: data.prl_photo,
                            imageURL:  `${URLIMAGE}${data.prl_photo}`,
                            id: body.id
                        };
                        response.message = "Success to get Photo Profile";
                        response.code = 100;
                        response.state = true
                        throw response;
                    }else{
                        response.data = {};
                        response.message = "Failed to get Photo Profile, profile not found";
                        response.code = 104;
                        response.state = false
                        throw response;
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    throw response;
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    getAll = (body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            try{
                let limit = 10;

                let start = (body.page - 1) * limit | 0;
                let count = await database.profile.connection.raw(`SELECT COUNT(*) FROM profile`);
                count = count.rows[0].count;
                let data = await database.profile.connection.raw(`
                SELECT 
                prl_nama as nama,
                prl_nohp as nohp,
                prl_username as username,
                prl_role as role,
                prl_saldo_nexus as saldo_nexus,
                prl_profile_id as id,
                prl_saldo as saldo
                FROM
                profile
                ORDER BY prl_created_at
                LIMIT ${limit} OFFSET ${start}
                `)

                let output = {
                    list: data.rows,
                    start: start,
                    total: count,
                    per_page: limit,
                    current_page: body.page
                }
                response.data = output
                response.code = 100;
                response.state = true;
                response.message = 'Success to Get All Profile'
                resolve(response);
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)

                response.data = {}
                response.code = 102;
                response.state = false;
                response.message = 'Failed to Get All Profile'
                resolve(response);
            }
        })
    }
    // getAllPemateri = () =>{
    //     let response = this.structure;
    //     return new Promise(async (resolve) => {
    //         try{
    //             // let pemateri = await database.profile.allSelect({prl_role: 'pemateri'});
    //             let pemateri = await database.profile.connection.raw(` SELECT
    //             prl_nama as nama,
    //             prl_nohp as nohp,
    //             prl_username as username,
    //             prl_role as role,
    //             prl_saldo_nexus as saldo_nexus,
    //             prl_profile_id as id
    //             FROM
    //             profile`)
    //             pemateri = pemateri.rows;
    //             response.data = pemateri
    //             response.code = 100;
    //             response.state = true;
    //             response.message = 'Success to Get All Profile'
    //             resolve(response);
    //         }catch(err){
    //             console.log(err);
    //             response.data = {}
    //             response.code = 102;
    //             response.state = false;
    //             response.message = 'Failed to Get All Pemateri'
    //             resolve(response);
    //         }
    //     });
    // }

    detailUser = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let data = await database.profile.connection.raw(`
                    SELECT
                    prl_nama as nama,
                    prl_nohp as nohp,
                    prl_username as username,
                    prl_role as role,
                    prl_saldo_nexus as saldo_nexus,
                    prl_profile_id as id,
                    prl_saldo as saldo,
                    prl_nik as nik,
                    prl_tanggal_lahir as tanggal_lahir,
                    prl_tempat_lahir as tempat_lahir,
                    prl_alamat as alamat,
                    prl_gender as gender,
                    prl_gelar as gelar,
                    prl_gelar_profesi as gelar_profesi,
                    prl_created_at as created,
                    prl_updated_at as updated,
                    prl_isactive as isactive,
                    COALESCE(prl_photo, prl_photo, 'img_default.PNG') as photo,
                    COALESCE(prl_photo, CONCAT('${URLIMAGE}', prl_photo), CONCAT('${URLIMAGE}', 'img_default.PNG')) as photolink
                    FROM
                    profile
                    WHERE
                    profile.prl_profile_id = '${body.profileid}'
                    `)
                    // prl_photo as photo,

                    if(data.rows.length > 0){
                        response.data = data.rows[0]
                        response.code = 100;
                        response.state = true;
                        response.message = 'Success to Get All Profile'
                        resolve(response);
                    }else{
                        response.data = data.rows[0]
                        response.code = 103;
                        response.state = false;
                        response.message = 'Failed, Profile Not Found'
                        throw response;
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    throw response;
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    search = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let data = await database.profile.connection.raw(
                        `
                        SELECT 
                        prl_nama as nama,
                        prl_nohp as nohp,
                        prl_username as username,
                        prl_role as role,
                        prl_saldo_nexus as saldo_nexus,
                        prl_profile_id as id,
                        prl_saldo as saldo
                        FROM profile
                        WHERE
                        LOWER(prl_profile_id) LIKE '%${body.search.toLowerCase()}%'
                        OR
                        LOWER(prl_nama) LIKE '%${body.search.toLowerCase()}%'
                        OR
                        LOWER(prl_nohp) LIKE '%${body.search.toLowerCase()}%'
                        OR
                        LOWER(prl_username) LIKE '%${body.search.toLowerCase()}%'
                        OR
                        LOWER(prl_role) LIKE '%${body.search.toLowerCase()}%'
                        AND
                        prl_isactive = 1
                        `
                        );
                    response.data = data.rows
                    response.code = 100;
                    response.state = true;
                    response.message = `Success Search, ${data.rows.length} Data Found`
                    resolve(response);
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                err.message = 'Something Error';
                err.data = err.stack;
                resolve(err);
            }
        });
    }

    changePassword = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let akun = await database.profile.allSelect({prl_profile_id: body.id, prl_password: this.createPassword(body.password), prl_isactive: 1});
                    if(akun.length > 0){

                        // let OTP = this.getKodeOTP(akun.prl_nohp);
                        // if(!OTP.state){
                        //     response.data = {};
                        //     response.message = 'Gagal Mendapatkan Kode OTP, silahkan tunggu beberapa saat lagi';
                        //     response.code = 104;
                        //     response.state = false;
                        //     resolve(response);
                        // }

                        // OTP = OTP.kode;

                        // let data = await client.messages
                        // .create({
                        //     from: 'whatsapp:+14155238886',
                        //     body: `Kode OTP untuk merubah password anda : ${OTP.kode}`,
                        //     to: `whatsapp:+${akun.prl_nohp}`
                        // })

                    }else{
                        response.data = body;
                        response.message = `Profile tidak ditemukan, cek kembali password yang dikirimkan`;
                        response.code = 103;
                        response.state = false
                        resolve(response)
                    }
                    // console.log(akun)
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                err.message = 'Something Error';
                err.data = err.stack;
                resolve(err);
            }
        });
    }

    suspend = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let data = await database.profile.updateOne({prl_profile_id: body.profileid}, {prl_isactive: 0});
                    response.data = data;
                    response.message = `Success Suspend User`;
                    response.code = 100;
                    response.state = true
                    resolve(response)
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                err.message = 'Something Error';
                err.data = err.stack;
                resolve(err);
            }
        });
    }

    delete = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let data = await database.profile.updateOne({prl_profile_id: body.profileid}, {prl_isactive: 2});
                    response.data = data;
                    response.message = `Success Delete User`;
                    response.code = 100;
                    response.state = true
                    resolve(response)
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                err.message = 'Something Error';
                err.data = err.stack;
                resolve(err);
            }
        });
    }

    requestChangePrimaryData = (fields, body) => {
         let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    // Validasi User
                    let akun = await database.profile.allSelect({prl_profile_id: body.id, prl_isactive: 1});
                    if(akun.length > 0){
                        let OTP, type;
                        if(body.type === 'email'){
                            OTP = await this.getKodeOTP(akun.prl_nohp);
                            type = 'Email';
                        }else if(body.type === 'nohp'){
                            OTP = await this.getKodeOTP(akun.prl_email);
                            type = 'Nomor Telepon';
                        }

                        if(!OTP.state){
                            response.data = {};
                            response.message = 'Gagal Mendapatkan Kode OTP, silahkan tunggu beberapa saat lagi';
                            response.code = 104;
                            response.state = false;
                            resolve(response);
                        }

                        OTP = OTP.kode;
                        let textOTP = `Kode OTP untuk merubah ${type} Anda adalah\n*${OTP}*\nHarap masukkan pada Aplikasi\nKode OTP Akan Expire dalam 30 Menit dari Sekarang`;

                        if(body.type === 'email'){
                            // Kirim ke WA
                            let data = await client.messages
                            .create({
                                from: `whatsapp:+${number}`,
                                body: textOTP,
                                to: `whatsapp:+${akun.prl_nohp}`
                            })
                            response.data = data;
                            response.state = true;
                            response.code = 100;
                            response.message = `Sukses Send Kode OTP untuk Request ubah ${type}`
                            resolve(response);
                        }else if(body.type === 'nohp'){
                            // Kirim ke Email
                            let retData = await EmailController.sendEmail(akun.prl_email, 'Ubah Password', textOTP, textOTP);
                            if(retData.state){
                                response.data = retData;
                                response.state = true;
                                response.code = 100;
                                response.message = `Sukses Send Kode OTP untuk Request ubah ${type}`
                                resolve(response);
                            }else{
                                throw retData;
                            }
                        }else{
                            throw 'Type tidak ditemukan';
                        }
                    }else{
                        response.data={}
                        response.message = 'Akun Tidak aktif, sedang di suspend, silahkan kirim pesan ke Customer Service Prexux';
                        response.code = 102;
                        response.state = false;
                        resolve(response);
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                let data;
                data.code = 503;
                data.state = false;
                data.message = 'Something Error';
                data.data = err.stack;
                resolve(data);
            }
        })
    }

    changePrimaryData = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let where = {
                        prl_profile_id: body.id,
                        prl_isactive: 1
                    };
                    let update;
                    if(body.type === 'email'){
                        update = {
                            prl_email: body.value,
                            prl_updated_at: this.createDate(0)
                        }
                    }else if(body.type === 'nohp'){
                        update = {
                            prl_nohp: body.value,
                            prl_updated_at: this.createDate(0)
                        }
                    }else{
                        throw 'Type tidak ditemukan';
                    }

                    let updateData = await database.profile.updateOne(where, update);
                    if(updateData.state){
                        response.data = body
                        response.code = 100;
                        response.state = true;
                        response.message = `Berhasil Update ${body.type}`
                        resolve(response);
                    }else{
                        response.code = 103;
                        response.state = false;
                        response.message = `Gagal Update ${body.type}, silahkan coba kembali`
                        resolve(response)
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                err.message = 'Something Error';
                err.data = err.stack;
                resolve(err);
            }
        });
    }

    selectRole = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    // let data=  await database.profile.allSelect({prl_isactive: 1, prl_role: body.role})
                    let data = await database.profile.connection.raw(`
                        SELECT
                        prl_nama as nama,
                        prl_nohp as nohp,
                        prl_username as username,
                        prl_role as role,
                        prl_saldo_nexus as saldo_nexus,
                        prl_profile_id as id,
                        prl_saldo as saldo
                        FROM
                        profile
                        WHERE prl_role = '${body.role}'
                        `)

                    response.data = data.rows
                    response.code = 100;
                    response.state = true;
                    response.message = `Berhasil mendapatkan user dengan Role : ${body.role}`
                    resolve(response);
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                err.message = 'Something Error';
                err.data = err.stack;
                resolve(err);
            }
        });
    }

    updateEmail = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    /*Sending Whatsapp OTP Code*/
                    let profile = await database.profile.allSelect({prl_profile_id: body.id});
                    if(profile.length > 0){
                        profile = profile[0];

                        let OTP = await this.getKodeOTP(profile.prl_nohp);
                        if(!OTP.state){
                            response.data = {};
                            response.message = 'Gagal Mendapatkan Kode OTP, silahkan tunggu beberapa saat lagi';
                            response.code = 104;
                            response.state = false;
                            resolve(response);
                        }

                        let data = await client.messages
                        .create({
                            from: 'whatsapp:+14155238886',
                            body: `Kode OTP untuk merubah email Anda : ${OTP.kode}`,
                            to: `whatsapp:+${parseInt(profile.prl_nohp)}`
                        })
                        console.log(data);

                        response.data = {
                            OTP: OTP.kode,
                            output: profile.prl_nohp
                        }
                        response.message = `Success Sending OTP Code to Whatsapp`;
                        response.code = 100;
                        response.state = true
                        resolve(response)
                    }else{
                        response.message = `Profile not Found`;
                        response.code = 103;
                        response.state = false
                        resolve(response)
                    }
                }else{
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                err.message = 'Something Error';
                err.data = err.stack;
                resolve(err);
            }
        });
    }

    updatePhoneNumber = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    let profile = await database.profile.allSelect({prl_profile_id: body.id});
                    if(profile.length > 0){
                        profile = profile[0];

                        let OTP = await this.getKodeOTP(profile.prl_nohp);
                        if(!OTP.state){
                            response.data = {};
                            response.message = 'Gagal Mendapatkan Kode OTP, silahkan tunggu beberapa saat lagi';
                            response.code = 104;
                            response.state = false;
                            resolve(response);
                        }

                        /*Getting OTP Kode*/
                        /*Sending Email*/
                        let textOTP = `Kode OTP untuk merubah Nomor Handphone Anda adalah\n${OTP.kode}\nHarap masukkan pada Aplikasi\nKode OTP Akan Expire dalam 30 Menit dari Sekarang`;
                        // console.log(profile.prl_email, textOTP)
                        let retData = await EmailController.sendEmail(profile.prl_email, 'Ubah Nomer Handphone', textOTP, textOTP);
                        // console.log(retData)
                        if(retData.state){
                            response.data = {};
                            response.state = true;
                            response.code = 100;
                            response.message = `Sukses Send Kode OTP untuk Request ubah Nomor Handphone`
                            resolve(response);
                        }else{
                            response.message = `Failed to send Email`;
                            response.code = 104;
                            response.state = false
                            resolve(response)
                        }
                    }else{
                        response.message = `Profile not Found`;
                        response.code = 103;
                        response.state = false
                        resolve(response)
                    }
                }else{
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                err.message = 'Something Error';
                err.data = err.stack;
                resolve(err);
            }
        });
    }

    confirmOtp = (fields, body) => {
        let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                    /*Checking OTP From Database*/
                    let profile = await database.profile.allSelect({prl_profile_id: body.id});
                    if(profile.length > 0){
                        profile = profile[0];
                        const query = `
                        SELECT * FROM
                        public.otp_list
                        WHERE
                        otp_kode LIKE '%${body.otp}%'
                        AND
                        otp_nohp = '${profile.prl_nohp}'
                        AND
                        otp_created_at LIKE '%${this.momentFormat(this.createDate(0), 'YYYY-MM-DD')}%'
                        AND
                        otp_status = 0
                        LIMIT 1
                        `;
                        let otpData = await database.otp_list.connection.raw(query)

                        if(otpData.rows.length > 0){
                            otpData = otpData.rows[0];
                            /*Update OTP Data = 1*/
                            /*Update email or number phone*/
                            await database.otp_list.updateOne({otp_nohp: otpData.otp_nohp, otp_kode: otpData.otp_kode}, {otp_status: 1});
                            let Updated = await database.profile.updateOne({prl_profile_id: profile.prl_profile_id}, {
                                [`prl_${body.type}`]: body.changedvalue
                            });
                            if(Updated.state){
                                response.data = body;
                                response.message = `Success Update Profile : ${body.type}`;
                                response.code = 100;
                                response.state = true;
                                resolve(response);
                            }else{
                                response.message = `Failed to Update ${body.type}`;
                                response.code = 105;
                                response.state = false
                                resolve(response)
                            }
                        }else{
                            response.data = {}
                            response.message = `OTP Expired or Not Found`;
                            response.code = 104;
                            response.state = false
                            resolve(response)
                        }

                    }else{
                        response.message = `Profile not Found`;
                        response.code = 103;
                        response.state = false
                        resolve(response)
                    }
                }else{
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                console.error(err)
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                err.code = 503;
                err.state = false;
                err.message = 'Something Error';
                err.data = err.stack;
                resolve(err);
            }
        });
    }
}

module.exports = new ProfileController