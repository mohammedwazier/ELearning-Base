const MainController = require('@Controllers/MainController');
const ApiHelper = require('@Helper/API');

const { SLACK } = require('@Config/Config');
const { API } = SLACK;

class SendErrorController extends MainController{
	api;
	constructor(){
		super();
		this.api = API;
	}

	message = (message = 'Message kosong') => {
		return new Promise(async resolve => {
			let data = {
				channel: '#prexux',
				username: 'Error Prexux',
				text: message,
				icon_emoji: ':ghost:'
			}
			let response = await ApiHelper.sendSlack(API, data);
			resolve(response)
		})
	}
}

module.exports = new SendErrorController;