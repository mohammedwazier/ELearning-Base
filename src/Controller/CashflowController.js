const database = require('@Model/index');
const MainController = require('@Controllers/MainController');
const SendErrorController = require('@Controllers/SendErrorController');
const { STRUCTURE } = require('@Config/Config');

class CashflowController extends MainController {
	structure;
    constructor(){
        super();
        this.structure = STRUCTURE;
    }

    getAllCashflow(fields, body){
    	let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
            	/*
            	Objective
            	1. List Transaksi Cashflow yang sudah di group
            	2. List Total Transaksi
            	3. List Transaksi Ambigu / Error
            	*/

                if(diff.length === 0){
                    let mainQuery = `
                        SELECT a.cf_refid,
                            SUM(a.cf_kredit) as kredit,
                            SUM(a.cf_debet) as debet,
                            a.cf_profile_id,
                            prl.prl_nama as nama_profil,
                            CASE WHEN SUM(a.cf_kredit) = SUM(a.cf_debet)
                            THEN 0
                            ELSE 1 END as status
                        FROM
                            cashflow a
                        JOIN (SELECT * FROM profile WHERE prl_isactive = 1) prl ON prl.prl_profile_id = a.cf_profile_id
                        GROUP BY
                            a.cf_refid, a.cf_profile_id, prl.prl_nama, a.cf_created_at
                        ORDER BY
                            a.cf_created_at ASC
                        `
                	let data = await database.cashflow.connection.raw(mainQuery)
                    let success=  await database.cashflow.connection.raw(`WITH cashflowTEmp AS (${mainQuery}) SELECT COUNT(status) FROM cashflowTEmp WHERE status = 1`);
                    let failed=  await database.cashflow.connection.raw(`WITH cashflowTEmp AS (${mainQuery}) SELECT COUNT(status) FROM cashflowTEmp WHERE status = 0`);

            		let ok = success.rows[0].count;
            		let fail = failed.rows[0].count;

            		let retData = [];

            		let strData = {
            			error: fail,
            			success: ok,
            			list: data.rows
            		}

            		response.data = strData;
                    response.message = `Success Get Data`;
                    response.code = 100;
                    response.state = true
                    resolve(response)

                }else{
                	response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                SendErrorController.message(`Error on CashflowController\n${err.stack}`)
                delete response.data;
                response.state = false;
                response.code = 500;
                response.message = 'Errors';
                resolve(response)
            }
        });
    }

    getSingleCashflow = (fields, body) => {
    	let response = this.structure;
        return new Promise(async (resolve) => {
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)
            try{
                if(diff.length === 0){
                	let cashflow = await database.cashflow.allSelect({cf_refid: body.id});
                	if(cashflow.length > 0){
                		for(let idx = 0; idx < cashflow.length; idx++){
                			let d = cashflow[idx];
                			let akun = await database.profile.single({prl_profile_id: d.cf_profile_id, prl_isactive: 1});

                			d.nama_profil = akun.prl_nama;
                			d.account_state = false;
                			if(d.cf_internal_acc.length > 1){
                				let account = await database.account.single({acc_noakun: d.cf_internal_acc});
                				d.nama_internal_akun = account.acc_nama;
                				d.account_state = true;
                			}
                		}

                		response.data = cashflow;
	                    response.message = `Success Get Data`;
	                    response.code = 100;
	                    response.state = true
	                    resolve(response)
                	}else{
                		response.data = {};
                        response.message = "Cashflow Not Found";
                        response.code = 103;
                        response.state = false
                        return resolve(response)
                	}
                }else{
                	response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    return resolve(response)
                }
            }catch(err){
            	SendErrorController.message(`Error on CashflowController\n${err.stack}`);
            	delete response.data;
            	response.state = false;
            	response.code = 500;
            	response.message = 'Errors';
            	resolve(response);
            }
        });
    }

    singleCashflow = (fields, body) => {
        return new Promise(async resolve => {
            try{
                let response = this.structure;
                let fieldCheck = await this.checkFields(fields, body)
                // format
                if(fieldCheck === 1){
                    let cashflowData = await database.deposit.connection.raw(`
                        SELECT
                        a.*,
                        prl.*
                        FROM cashflow a
                        JOIN (SELECT * FROM profile) prl ON prl.prl_profile_id = a.cf_profile_id
                        WHERE cf_refid = '${body.refid}'
                        ORDER BY cf_debet DESC, cf_kredit DESC
                        `)
                    response.data = cashflowData.rows
                    response.message = `Success`
                    response.code = 100
                    response.state = true
                    resolve(response)
                }else if(fieldCheck === 0){
                    response.data = body
                    response.message = `Input Not Valid, Missing Parameter`
                    response.code = 102
                    response.state = false
                    resolve(response)
                }else{
                    response.data = body
                    response.message = `Something Error`
                    response.code = 500
                    response.state = false
                    resolve(response)
                }
            }catch(err){
                SendErrorController.message(`Error on CashflowController\n${err.stack}`)
                delete response.data;
                response.state = false;
                response.code = 500;
                response.message = 'Errors';
                resolve(response)
            }
        })
    }
}

module.exports = new CashflowController