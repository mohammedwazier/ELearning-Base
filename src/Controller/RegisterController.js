const { STRUCTURE, WHATSAPP } = require('@Config/Config');
const database = require('@Model/index');
const NIK = require('@Controllers/NikParse');
const { profileSelect, kodeOtpSelect } = require('@Query/QueryModel');
const API = require('@Helper/API');
const SendErrorController = require('@Controllers/SendErrorController');


const { accountSid, authToken } = WHATSAPP;

const client = require('twilio')(accountSid, authToken);
const MessagingResponse = require('twilio').twiml.MessagingResponse;

const MainController = require('@Controllers/MainController');
const EventController = require('@Controllers/EventController');

class RegisterController extends MainController {
    structure;
    constructor(){
        super();
        this.structure = STRUCTURE;
        this.apiKey = "b4ee981f060af05f5e8eead7932ec9364db107e7";
    }

    registerUserDashboard = async (fields, body) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)

            let role = newBody.indexOf('tipe') === -1 ? 'user' : body.tipe;
            if(Number(role) === 1){
                role = 'user';
            }

            let group = newBody.indexOf('group') ? 'prexux' : body.group;
            try{
                if(diff.length === 0){
                    let profileData = {
                        prl_nohp: body.nohp,
                        prl_username: body.username,
                        prl_profile_id: this.generateID(),
                        prl_email : body.email,
                        prl_isactive: 1,
                    }

                    let validate = await database.profile.connection.raw(profileSelect(profileData));

                    if(validate.rows.length > 0){
                        response.data = body
                        response.message = 'User Exists'
                        response.state = false;
                        response.code = 104;
                        return resolve(response)
                    }else{
                        profileData = {
                            ...profileData,
                            prl_nama: body.nama,
                            prl_role: role,
                            prl_tanggal_lahir: body.tanggallahir,
                            prl_tempat_lahir: body.tempatlahir,
                            prl_alamat: body.alamat,
                            prl_gender: body.gender,
                            prl_photo: body.photo,
                            prl_password: this.createPassword(body.password),
                        }
                        let result = await database.profile.insertOne(profileData);
                        let event = await EventController.checkingEvent('register', this.createDate(0));
                        if(event.state){
                            // Create Event;
                            event = event.data;
                            await EventController.createEventInbox(profileData.prl_profile_id, event.event_id, event.event_kode, event.event_value, event.event_nama);
                        }

                        if(result.state){
                            response.data = {
                                username: profileData.prl_username,
                                nama: profileData.prl_nama
                            }
                            response.message = `Success to Create user ${profileData.prl_nama}`
                            response.state = true;
                            response.code = 100;
                            resolve(response)
                        }else{
                            response.data = {}
                            response.message = 'Failed to Create Profile'
                            response.state = false;
                            response.code = 103;
                            resolve(response)
                        }
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }

            }catch(err){
                SendErrorController.message(`Error on RegisterController\n${err.stack}`)
                response.code = 503;
                response.state = false;
                response.message = 'Something wrong, please check'
                response.data = [];
                resolve(response);
            }

        })
    }

    registerUser = async (fields, body) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)

            let role = newBody.indexOf('tipe') === -1 ? 'user' : body.tipe;
            if(Number(role) === 1){
                role = 'user';
            }

            let default_photo = await database.setting.single({st_kode: 'profile_default'})

            if(diff.length === 0){
                const profileData = {
                    prl_nik: body.nik,
                    prl_nama: body.nama,
                    prl_nohp: body.nohp,
                    prl_username: body.username,
                    prl_password: this.createPassword(body.password),
                    prl_isactive: 1,
                    prl_profile_id: this.generateID(),
                    prl_role: role,
                    prl_photo: default_photo.st_value,
                    prl_group: body.group
                }

                let validate = await database.profile.connection.raw(profileSelect(profileData));

                if(validate.rows.length > 0){
                    response.data = body
                    response.message = 'User Exists'
                    response.state = false;
                    response.code = 104;
                    return resolve(response)
                }else{
                    let result = await database.profile.insertOne(profileData);
                    await database.otp_list.updateOne({otp_nohp: body.nohp, otp_kode: body.otp, otp_expired_at: this.createDate(30, 'minutes')}, {otp_status: 1})
                    if(result.state){
                        response.data = {
                            username: profileData.prl_username,
                            nama: profileData.prl_nama
                        }
                        // Update Kode OTP Menjadi 1
                        response.message = `Success to Create user ${profileData.prl_nama}`
                        response.state = true;
                        response.code = 100;
                        resolve(response)
                    }else{
                        response.data = {}
                        response.message = 'Failed to Create Profile'
                        response.state = false;
                        response.code = 103;
                        resolve(response)
                    }
                }
            }else{
                response.data = {};
                response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                response.code = 102;
                response.state = false
                resolve(response);
            }
        })
    }

    validasi = (tipe, data) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            try{
               /* if(data.tipe === 'nik'){
                    let nik = NIK(data.value);
                    let newNik = new Map(Object.entries(nik));
                    if(newNik.has('error')){
                        response.code = 103
                        response.message = `${data.value} tidak Valid`
                        response.data = {};
                        response.state = false;
                        throw response;
                    }
                }*/
                let newBody = new Map(Object.entries(data));
                if(!newBody.has('group')){
                    response.code = 102
                    response.message = `Missing Parameter Group`
                    response.state = false;
                    response.data = {};
                    throw response;
                }
                if(data.tipe === 'nohp'){
                    if(data.value.substring(0,2) !== '62'){
                        response.code = 104;
                        response.message = `${data.value}, nomor tidak valid, harus menggunakan 62 tanpa +, silahkan cek kembali`
                        response.data = {}
                        response.state = false;
                        throw response;
                    }
                }
                let status = tipe.includes(data.tipe)
                if(status){
                    let res;
                    if(data.tipe === 'otp'){
                        const where = { otp_kode: data.value, otp_status: 0 };
                        res = await database.otp_list.allSelect(where)
                        if(res.length > 0){
                            res = res[0];
                            const retData = {
                                nohp: res.otp_nohp,
                                otp: res.otp_kode
                            }
                            response.code = 100
                            response.message = `OTP Valid`
                            response.state = true;
                            response.data = retData;
                            resolve(response)
                        }else{
                            response.code = 103
                            response.message = `OTP Not Exist`
                            response.state = false;
                            response.data = {};
                            throw response;
                        }
                    }else{
                        let fields = `prl_${data.tipe}`;
                        const where = {
                            [fields] : data.value,
                            prl_group: data.group,
                            prl_isactive: 1
                        };
                        res = await database.profile.allSelect(where);
                        if(res.length === 0){
                            response.state = true;
                            response.message = `${data.value} are valid to register`;
                            response.code = 100;
                            response.data = data;
                            resolve(response);
                        }else{
                            response.code = 102
                            response.message = `${data.value} was exist`
                            response.state = false;
                            response.data = {};
                            throw response;
                        }
                    }

                }else{
                    response.code = 105;
                    response.state = false;
                    response.data = {};
                    response.message = `Data not Valid`;
                    throw response;
                }
            }catch(errRes){
                SendErrorController.message(`Error on RegisterController\n${errRes.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        })
    }
    registerWhatsapp = (fields, body) => {
        return new Promise(async (resolve) => {
            console.log(body)
            let response = this.structure;
            let newBody = Object.keys(body);
            let diff = fields.filter((x) => newBody.indexOf(x) === -1)

            if(diff.length === 0){
                for (let idx = 0; idx < body.message.length; idx++) {
                    let number = body.sender[idx];
                    let msg = body.message[idx];
                    let urlWhatsapp = `https://api.autochat.id/api/message/send`;

                    if (msg.indexOf('reg') > -1 && msg.indexOf('xux') > -1) { //Jika ada
                        const valNomer = await database.profile.allSelect({
                            prl_nohp: number,
                            prl_isactive: 1
                        })

                        if (valNomer.length > 0) {
                            response.code = 101;
                            response.state = false;
                            response.data = {};
                            response.message = `Nomor : +${number} telah terdaftar pada aplikasi`;

                            const msgOut = {
                                phone: number,
                                name: "prexux",
                                message: response.message
                            }

                            let result = await API.post(urlWhatsapp, msgOut, {
                                'x-api-key': this.apiKey
                            });

                            console.log(result)

                            return resolve(result)
                        } else {
                            let OTP = await this.getKodeOTP(getNumber);
                            if (!OTP) {
                                const msgOut = {
                                    phone: number,
                                    name: "prexux",
                                    message: "Gagal mendapatkan Kode OTP, silahkan coba beberapa saat lagi"
                                }

                                let result = await API.post(urlWhatsapp, msgOut, {
                                    'x-api-key': this.apiKey
                                });

                                console.log(result)

                                return resolve(result)
                            }
                            OTP = OTP.kode;
                            const message = `Kode OTP anda adalah : \n${OTP}`;

                            const msgOut = {
                                phone: number,
                                name: "prexux",
                                message: message
                            }

                            let result = await API.post(urlWhatsapp, msgOut, {
                                'x-api-key': this.apiKey
                            });
                            console.log(result)
                            return resolve(result)
                        }

                    } else if (msg.indexOf('reg') > -1 && msg.indexOf('pgri') > -1) {
                        let url = `https://appepgri.nenggala.id/wabot/hit.php`;
                        let result = await API.post(url, body);

                        console.log(result)

                        if (result.data) {
                            response.data = null;
                            response.message = `Success sending Message`;
                            response.code = 110;
                            response.state = true;
                            return resolve(response);
                        } else {
                            response.data = null;
                            response.message = `Failed sending Message`;
                            response.code = 110;
                            response.state = false;
                            return resolve(response);
                        }
                    }else{
                        /* Sending Message, Command tidak ditemukan */
                        /* Sending API */
                        const msgOut = {
                            phone: number,
                            name: "prexux",
                            message: "Command tidak ditemukan"
                        }

                        console.log(urlWhatsapp);

                        let result = await API.post(urlWhatsapp, msgOut, {
                            'x-api-key': this.apiKey
                        });
                        console.log(result)
                        return resolve(result)
                    }
                }
            }else{
                response.data = {};
                response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                response.code = 102;
                response.state = false
                return resolve(response);
            }
        })
    }

    forgotPassword = (fields, body) => {
         return new Promise(async (resolve) => {
            let response = this.structure;
            let newBody = Object.keys(body);
            try{
                let diff = fields.filter((x) => newBody.indexOf(x) === -1)
                if(diff.length === 0){
                    let data = [];
                    let email = await database.profile.allSelect({prl_email: body.value});
                    if(email.length > 0 ){
                        data = email;
                    }
                    let username = await database.profile.allSelect({prl_username: body.value})
                    if(username.length > 0){
                        data = username;
                    }
                    if(body.value.constructor === number){
                        let phone = await database.profile.allSelect({prl_nohp: body.value});
                        if(phone.length > 0){
                            data = phone;
                        }
                    }
                    data = data[0];
                    // Generate OTP
                    // Kirim ke WA,
                    // Validasi ke 
                    // Set new Pin Password

                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on ProfileController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    testSendWa = () => {
        return new Promise(async resolve => {
            let data = await client.messages
            .create({
                from: 'whatsapp:+14155238886',
                body: `It's taco time!`,
                to: 'whatsapp:+6282210072854'
            })
            // .then(message => console.log(message));
            resolve(data)

        })
    }
}

module.exports = new RegisterController;