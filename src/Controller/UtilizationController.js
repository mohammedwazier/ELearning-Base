const { STRUCTURE, URLDATA, URLIMAGE, VERSION } = require('@Config/Config');
const database = require('@Model/index');

let MainController = require('@Controllers/MainController');
const SendErrorController = require('@Controllers/SendErrorController');

class UtilizationController extends MainController {
    structure;
    constructor(){
        super();
        this.structure = STRUCTURE;
    }

    getNumber = () => {
    	return new Promise(async resolve => {
	    	let response = this.structure;
	    	let setting = await database.setting.single({st_kode: 'wa_bot'});
	        let wa = setting.st_value;

     		setting = await database.setting.single({st_kode: 'cs_number'});
	        let csnumber = setting.st_value;

	        let pgri = await database.setting.single({st_kode: 'pgri_login'});
	        pgri = pgri.st_value;

	        let prexux = await database.setting.single({st_kode: 'prexux_login'});
	        prexux = prexux.st_value;

	        response.state = true;
	        response.code = 100;
	        response.message = 'Success get Number WA';
	        response.data = {
	        	whatsapp: wa,
	        	customerservice: csnumber,
	        	pgri_login: Number(pgri),
	        	prexux_login: Number(prexux)
	        }

	        resolve(response);
    	})
    }

    validatePassword = (body) => {
    	return new Promise(async (resolve) => {
    		let response = this.structure
    		let data = await database.profile.allSelect({prl_profile_id: body.username, prl_password: this.createPassword(body.password), prl_isactive: 1});
    		if(data.length > 0){
    			delete body.password;
    			response.state = true;
    			response.code = 100;
    			response.message = 'Data Valid'
    			response.data = body;
    			resolve(response);
    		}else{
    			response.state = false;
    			response.code = 101;
    			response.message = 'Not Valid'
    			response.data = {};
    			resolve(response);
    		}
    	})
    }

    listSetting = (fields, body) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            try{
                let newBody = Object.keys(body);
                let diff = fields.filter((x) => newBody.indexOf(x) === -1)
                if(diff.length === 0){
                    let data = await database.setting.all();
                    response.data = data;
                    response.message = `Success get list Setting`;
                    response.code = 100;
                    response.state = true
                    resolve(response);
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on UtilizationController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    singleSetting = (fields, body) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            try{
                let newBody = Object.keys(body);
                let diff = fields.filter((x) => newBody.indexOf(x) === -1)
                if(diff.length === 0){
                    let data = await database.setting.single({st_kode: body.kode});
                    response.data = data;
                    response.message = `Success get Setting For Code : ${body.kode}`;
                    response.code = 100;
                    response.state = true
                    resolve(response);
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on UtilizationController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    createSetting = (fields, body) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            try{
                let newBody = Object.keys(body);
                let diff = fields.filter((x) => newBody.indexOf(x) === -1)
                if(diff.length === 0){
                    let insertData = {
                        st_keterangan: body.keterangan,
                        st_kode: body.kode,
                        st_value: body.value,
                        st_created_at: this.createDate(0),
                        st_id_admin: body.id,
                        st_isactive: body.active,
                        st_typevalue: body.typevalue
                    }
                    let insert = await database.setting.insertOne(insertData);
                    if(insert.state){
                        response.data = body;
                        response.message = `Success insert new Setting`;
                        response.code = 100;
                        response.state = true
                        resolve(response);
                    }else{
                        response.data = body;
                        response.message = `Failed to insert new Setting`;
                        response.code = 103;
                        response.state = false
                        resolve(response);
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on UtilizationController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    updateSetting = (fields, body) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            try{
                let newBody = Object.keys(body);
                let diff = fields.filter((x) => newBody.indexOf(x) === -1)
                if(diff.length === 0){
                    let update = {
                        where: {
                            st_id: body.idsetting,
                        },
                        update: {
                            st_keterangan: body.keterangan,
                            st_kode: body.kode,
                            st_value: body.value,
                            st_updated_at: this.createDate(0),
                            st_id_admin: body.id,
                            st_isactive: body.active,
                            st_typevalue: body.typevalue
                        }
                    }

                    let updated = await database.setting.updateOne(update.where, update.update);
                    if(updated.state){
                        response.data = body;
                        response.message = `Success Update Setting`;
                        response.code = 100;
                        response.state = true
                        resolve(response);
                    }else{
                        response.data = body;
                        response.message = `Failed to Update Setting`;
                        response.code = 103;
                        response.state = false
                        resolve(response);
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on UtilizationController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    Homepage = (fields, body) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            try{
                let newBody = Object.keys(body);
                let diff = fields.filter((x) => newBody.indexOf(x) === -1)
                if(diff.length === 0){
                    let data = await database.setting.connection.raw(`
                        select
                        (SELECT COUNT(*) from profile) as user,
                        (SELECT COUNT(*) from produk) as produk,
                        (SELECT COUNT(*) FROM transaksi WHERE trx_status = 'S') as transaksi_sukses,
                        (SELECT COUNT(*) FROM transaksi WHERE trx_status = 'X') as transaksi_gagal,
                        (SELECT COUNT(*) FROM transaksi WHERE trx_status IN ('Q', 'W', 'P')) as transaksi_proses,
                        (SELECT COUNT(*) FROM profile WHERE prl_profile_id IN (SELECT trx_id_profile FROM transaksi)) as user_aktif
                        `)
                    response.data = data.rows[data.rows.length - 1];
                    response.message = `Success get Statistic for Homepage Data`;
                    response.code = 100;
                    response.state = true
                    resolve(response);
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on UtilizationController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    PrivacyPolicy = () => {
        return new Promise(async resolve => {
            let response = this.structure;
            try{
                let data = await database.setting.single({st_kode: 'pri_pol'});
                response.data = data;
                response.message = `Success get Privacy Policy HTML Page`;
                response.code = 100;
                response.state = true
                resolve(response);
            }catch(err){
                SendErrorController.message(`Error on UtilizationController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        })
    }

    getListUserGuide = () => {
        return new Promise(async resolve => {
            let response = this.structure;
            try{
                // let data = await database.userguide.all();
                let data = await database.userguide.connection.raw(`
                    SELECT
                    ug_id as guideid,
                    ug_name as guidename,
                    ug_file as file,
                    CONCAT('${URLIMAGE}', ug_file) as guidelink
                    FROM userguide ORDER BY ug_created_at DESC
                    `);
                response.data = data.rows;
                response.message = `Success get UserGuide List`;
                response.code = 100;
                response.state = true;
                resolve(response);
            }catch(err){
                SendErrorController.message(`Error on UtilizationController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        })
    }

    CreateUserguide = (fields, body) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            try{
                let newBody = Object.keys(body);
                let diff = fields.filter((x) => newBody.indexOf(x) === -1)
                if(diff.length === 0){
                    const userguide = {
                        ug_id: this.generateID(),
                        ug_name: body.name,
                        ug_file: body.file,
                        ug_created_at: this.createDate(0)
                    }
                    const insert = await database.userguide.insertOne(userguide);
                    if(insert.state){
                        response.data = body;
                        response.message = `Success insert new UserGuide`;
                        response.code = 100;
                        response.state = true
                        resolve(response);
                    }else{
                        response.message = `Failed to insert new UserGuide`;
                        response.code = 103;
                        response.state = false;
                        resolve(response);
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false;
                    resolve(response);
                }
            }catch(err){
                SendErrorController.message(`Error on UtilizationController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }

    DeleteUserguide = (fields, body) => {
        return new Promise(async (resolve) => {
            let response = this.structure;
            try{
                let newBody = Object.keys(body);
                let diff = fields.filter((x) => newBody.indexOf(x) === -1)
                if(diff.length === 0){
                    let DeleteGuide = await database.userguide.deleteOne({ug_id: body.guideid});
                    console.log(DeleteGuide)
                    if(DeleteGuide.state){
                        response.data = body
                        response.message = `Success delete UserGuide`;
                        response.code = 100;
                        response.state = true
                        resolve(response);
                    }else{
                        response.message = `Failed delete UserGuide`;
                        response.code = 103;
                        response.state = false
                        resolve(response);
                    }
                }else{
                    response.data = {};
                    response.message = `Input Not Valid, Missing Parameter : '${diff.toString()}'`;
                    response.code = 102;
                    response.state = false
                    resolve(response);
                }
            }catch($err){
                SendErrorController.message(`Error on UtilizationController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }
}

module.exports = new UtilizationController;