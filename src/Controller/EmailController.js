const database = require('@Model/index');
const MainController = require('@Controllers/MainController');
const { STRUCTURE, URLIMAGE } = require('@Config/Config');
const SendErrorController = require('@Controllers/SendErrorController');

const nodemailer = require('nodemailer');

class EmailController extends MainController{
	structure;
	mailer;
	constructor(){
		super();
		this.structure = STRUCTURE
	}

	sendEmail = (to, subject, text, html) => {
		let response = this.structure;
		return new Promise(async (resolve) => {
			try{
				if(to === undefined || subject === undefined || text === undefined || html === undefined){
					response.code = 500;
					response.state = false;
					response.message = 'Parameter Input not Valid';
					resolve(response);
				}

				// MAIL_USERNAME=dummy.wazier@gmail.com
				// MAIL_PASSWORD=qlikoodzsmrwekll

				let testAccount = await nodemailer.createTestAccount();

				let transporter = nodemailer.createTransport({
					service: 'gmail',
					auth: {
						user: 'dummy.wazier@gmail.com',
						pass: 'qlikoodzsmrwekll'
					}
				})
				// let transporter = nodemailer.createTransport({
				//     host: "smtp.ethereal.email",
				//     port: 587,
				//     secure: false, // true for 465, false for other ports
				//     auth: {
				//       	user: testAccount.user, // generated ethereal user
				//       	pass: testAccount.pass, // generated ethereal password
				//     },
			 //  	});

				let sendingMessage = await transporter.sendMail({
					from: "Prexux <no-reply@gmail.com>",
					to: to,
					subject: subject,
					text: text,
					html: html
				})

				// console.log(sendingMessage)

				response.data = sendingMessage;
				response.code = 100;
				response.state = true;
				response.message = "Message Sent";
				resolve(response);
			}catch(err){
				SendErrorController.message(`Error on EmailController\n${err.stack}`)
				delete response.data
				response.code = 500;
				response.state = false;
				response.message = "Errors";
				resolve(response);
			}
		})
	}
}

module.exports = new EmailController;