const database = require('@Model/index');
const MainController = require('@Controllers/MainController');
const { STRUCTURE } = require('@Config/Config');
const SendErrorController = require('@Controllers/SendErrorController');

const Helper = require('@Helper/API');

class MutasiBankController extends MainController {
	structure;
    constructor(){
        super();
        this.structure = STRUCTURE;
    }

    sendingToTokenPadi = (body) => {
        return new Promise(async resolve => {

            await SendErrorController.message(json_encode(body));
            const requestApi  = await Helper.post('https://beapi.tokenpadi.com/tokenpadi_prod/a_door.php', {
                action: 'payment_report',
                content: body
            });
            resolve(requestApi);
        })
    }

    insertMutasi = (fields, body) => {
    	let response = this.structure;
        return new Promise(async (resolve) => {
            try{
                console.log('Incoming Topup', body)
                await this.sendingToTokenPadi(body);

                let data = await database.mutasi_bank.insertOne({mutasi_raw: JSON.stringify(body)});
        		response.data = body;
        		response.message = 'Berhasil Insert Mutasi Data';
        		response.code = 100;
        		response.state = true;
        		resolve(response);
            }catch(err){
                SendErrorController.message(`Error on MutasiBankController\n${err.stack}`)
                response.code = 503;
                response.state = false
                resolve(response);
            }
        });
    }
}

module.exports = new MutasiBankController