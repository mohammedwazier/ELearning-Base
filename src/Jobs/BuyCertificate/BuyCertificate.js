
require('module-alias/register')

const { STRUCTURE } = require('@Config/Config');
const database = require('@Model/index');
let MainController = require('@Controllers/MainController');
const SendErrorController = require('@Controllers/SendErrorController');
const moment = require('moment-timezone');

var Jimp = require('jimp')
const fs = require('fs')
const path = require('path')

MainController = new MainController();

const processing = async () => {
    return new Promise(async (resolve) => {
        let data = await database.inbox.allSelect({ibx_tipe: 'BUYCERTIFICATE', 'ibx_status': 'Q'});


        let setting = await database.setting.single({st_kode: 'fee_produk'});
        let feepersen = Number(setting.st_value);

        let ibxSucc = new Array();

        if(data.length === 0){
            return resolve(true);
        }else{
            try{
                for(let idx = 0; idx < data.length; idx++){
                    let inbox = data[idx];

                    // await database.inbox.updateOne({ibx_refid: inbox.ibx_refid}, {ibx_status: 'P'});

                    let FormatMsg = MainController.FormatMsg(inbox.ibx_format_msg.split('.'));

                    let produk = await database.produk.single({produk_id: FormatMsg.productid, produk_kodeProduk: FormatMsg.kode});
                    let akun = await database.profile.single({prl_profile_id: FormatMsg.profileid, prl_isactive: 1});
                    let transaksi = await database.transaksi.allSelect({trx_refid: inbox.ibx_refid})
                    var placement = await database.placement.single({placement_produkid: FormatMsg.productid});
                    
                    placement = JSON.parse(placement.placement_data);

                    if(transaksi.length === 0){
                        await database.inbox.updateOne({ibx_refid: inbox.ibx_refid}, {ibx_status: 'G'});
                        continue;
                    }
                    transaksi = transaksi[0];
                    let nexus = Number(produk.produk_harga)

                    let feeNexus = nexus * (feepersen / 100);
                    let keuntunganUserNexus = nexus - feeNexus;

                    let jurnal1 = {
                        cf_keterangan: `Pengurangan Nexus dari profile ${akun.prl_profile_id} sebesar ${produk.produk_harga} Nexus untuk pembelian Certificate dengan ID : ${produk.produk_id}`,
                        cf_tipe: 'buy',
                        cf_kredit: 0,
                        cf_debet: nexus,
                        cf_nominal: produk.produk_harga + ' Nexus',
                        cf_refid: inbox.ibx_refid,
                        cf_internal_acc: '',
                        cf_profile_id: akun.prl_profile_id,
                        cf_mode: 'min_user'
                    }

                    let jurnal2 = {
                        cf_keterangan: `Penambahan Penjualan ke Profile pemateri dengan id : ${produk.produk_pemateri_id} dengan pembelian Sertifikat ${produk.produk_id} seharga ${nexus} Nexus`,
                        cf_tipe: 'buy',
                        cf_kredit: keuntunganUserNexus,
                        cf_debet: 0,
                        cf_nominal: nexus + ' Nexus',
                        cf_refid: inbox.ibx_refid,
                        cf_profile_id: akun.prl_profile_id,
                        cf_mode: 'add_pemateri'
                    }

                    const penampung = '20200605171724213363'
                    
                    let jurnal3 = {
                        cf_keterangan: `Penambahan ke account Penampungan Keuntungan Sertifikat dengan id penampung: '${penampung}' degan pembelian Sertifikat seharga ${nexus} Nexus dengan keuntungan sebesar ${feeNexus}`,
                        cf_tipe: 'buy',
                        cf_kredit: feeNexus,
                        cf_debet: 0,
                        cf_nominal: nexus + ' Nexus',
                        cf_refid: inbox.ibx_refid,
                        cf_internal_acc: penampung,
                        cf_profile_id: akun.prl_profile_id,
                        cf_mode: 'add_profit'
                    }


                    let nameCert = `${MainController.generateID()}_${akun.prl_profile_id}_${produk.produk_certificate}`;
                    let nomorCert = await database.transaksi.connection.raw(`SELECT COUNT(trx_id) as certNumber FROM transaksi WHERE trx_produk_id = '${produk.produk_id}' AND trx_status = 'S'`)
                    nomorCert = nomorCert.rows[0];
                            
                    try{
                        let imgSource = `../../Source/${produk.produk_certificate}`;
                        let imgPath = path.join(__dirname, imgSource);


                        // let nameCert = `${MainController.generateID()}_${akun.prl_profile_id}_${produk.produk_certificate}`;
                        let nameExport = `../../Source/${nameCert}`;
                        let exportFile = path.join(__dirname, nameExport);
                        
                        const image = await Jimp.read(imgPath);
                        
                        let { width, height } = image.bitmap;

                        
                        
                        if(width === placement.image.original.width){
                            convertReal = Number((width / placement.image.resizeTransform.width).toFixed(1));
                        }else{
                            convertReal = width / placement.image.original.width;
                        }
                        
                        console.log('conver real', convertReal)
                        let sizeText = Math.floor(placement.nama.fontSize * convertReal);
                        let fontSource = `../../Source/font_storage/b_cert_${sizeText}.fnt`

                        let fontPath = path.join(__dirname, fontSource);
                        console.log(fontPath)
                        const font = await Jimp.loadFont(fontPath);

                        sizeText = Math.floor(placement.nomor.fontSize * convertReal)
                        let numberFont =  `../../Source/font_storage/nomor_${sizeText}.fnt`
                        numberFont = path.join(__dirname, numberFont);
                        numberFont = await Jimp.loadFont(numberFont);


                        // let nomorCert = await database.transaksi.connection.raw(`SELECT COUNT(trx_id) as certNumber FROM transaksi WHERE trx_produk_id = '${FormatMsg.produkid}' AND trx_status = 'S'`)

                        nomorCert = Number(nomorCert.certnumber);

                        if(nomorCert === 0){
                            nomorCert = `0001`;
                        }else if(nomorCert < 10){
                            nomorCert = `000${nomorCert}`;
                        }else if(nomorCert < 100){
                            nomorCert = `00${nomorCert}`;
                        }else if(nomorCert < 1000){
                            nomorCert = `0${nomorCert}`;
                        }else{
                            nomorCert = `${nomorCert}`;
                        }

                        let nomor = `PREI/SEM/${produk.produk_kodeProduk}/${MainController.romanize(moment().tz("Asia/Jakarta").format("MM"))}/${moment().tz("Asia/Jakarta").format("YYY")}/${nomorCert}`
                        let namaText = akun.prl_nama;

                        var textWidth = Jimp.measureText(font, namaText);
                        var textHeight = Jimp.measureTextHeight(font, namaText);

                        let nomorWidth = Jimp.measureText(font, nomor);
                        let nomorHeight = Jimp.measureTextHeight(font, nomor);

                        let textLeft = placement.nama.left * convertReal;
                        let textTop = placement.nama.top * convertReal;

                        /* let centerX = (textLeft + (textWidth / 2)) - (textWidth / 2) + 2;
                        let centerY = (textTop + (textHeight - (textHeight / 2))) + 2; */

                        let centerX = (textLeft + (textWidth / 2)) - (textWidth / 2) + 2;
                        let centerY = (textTop + (textHeight / 2)) + 2;

                        let textNumLeft = placement.nomor.left * convertReal;
                        let textNumTop = placement.nomor.top * convertReal;

                        // let centerNumX = (textNumLeft + (nomorWidth / 2)) - (nomorWidth / 2 + 2)
                        // let centerNumY = (textNumTop + (nomorHeight / 2)) - (nomorHeight / 2) + 2;

                        let centerNumX = (textNumLeft + (nomorWidth / 2)) - (nomorWidth / 2 + 2)
                        let centerNumY = (textNumTop);

                        await image.print(font, centerX, centerY, {
                            text: namaText,
                            alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
                            alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
                        }, textWidth, textHeight);
                        await image.print(numberFont, centerNumX, centerNumY, nomor, nomorWidth)
                        await image.writeAsync(exportFile); //End

                    }catch(errors){
                        console.log(errors);
                        SendErrorController.message(`Error on Cron Buy Certificate\n${errors.stack}`)
                        continue;
                    }

                    /* Process Jurnaling and Transaksi */

                    let query1 = `UPDATE profile SET prl_saldo_nexus = prl_saldo_nexus - ${nexus} WHERE prl_profile_id = '${akun.prl_profile_id}' AND prl_isactive = 1`;
                    await database.profile.connection.raw(query1);

                    const query2 = `UPDATE profile SET prl_wallet_pemateri  = prl_wallet_pemateri + ${keuntunganUserNexus} WHERE prl_profile_id = '${produk.produk_pemateri_id}'`
                    await database.profile.connection.raw(query2);
                    
                    const query3 = `UPDATE account SET acc_saldo = acc_saldo + ${feeNexus} WHERE acc_noakun = '${penampung}'`
                    await database.account.connection.raw(query3)

                    const penampungBeli = '20200507215106956376';
                    const query4 = `UPDATE account SET acc_saldo = acc_saldo + ${keuntunganUserNexus} WHERE acc_noakun = '${penampungBeli}'`;
                    await database.account.connection.raw(query4)

                    let cashflow = await database.cashflow.insert([jurnal1, jurnal2, jurnal3])

                    if(cashflow.state){
                        let keteranganTrx = `Berhasil membeli Sertifikat ${produk.produk_namaProduk}, sertifikat dapat di download pada halaman Library`;
                        let trxData = {
                            certificate: nameCert,
                            created: MainController.createDate(0),
                            download: 0,
                            access: 0
                        }
                        await database.transaksi.updateOne({trx_id: transaksi.trx_id, trx_invoice: transaksi.trx_invoice, trx_refid: inbox.ibx_refid}, {trx_saldo_after: akun.prl_saldo_nexus - nexus, trx_status: 'S', trx_keterangan: keteranganTrx, trx_updated_at: MainController.createDate(0),trx_data: JSON.stringify(trxData)})
                        await database.inbox.updateOne({ibx_refid: inbox.ibx_refid}, {ibx_status: 'S'});
                        let Outbox = {
                            obx_refid: inbox.ibx_refid,
                            obx_id_profile: akun.prl_profile_id,
                            obx_interface: 'H',
                            obx_tipe: 'BUYCERITICATE',
                            obx_status: 'S',
                            obx_format_msg: inbox.ibx_format_msg,
                            obx_keterangan: `Berhasil input ke Outbox pada ${MainController.createDate(0)}`,
                            obx_raw_data: JSON.stringify(transaksi)
                        }
                        await database.outbox.insertOne(Outbox)
                        await database.produk.connection.raw(`UPDATE produk SET produk_buy = produk_buy + 1 WHERE produk_id = '${produk.produk_id}' AND produk."produk_kodeProduk" = '${produk.produk_kodeProduk}'`)
                        let notifData = {
                            data: {
                            id: akun.prl_profile_id,
                            title: 'Berhasil membeli Sertifikat',
                            message: keteranganTrx,
                            nama_sender: 'Prexux',
                            menu: 'trx',
                            tipe: 'default',

                            send: 'user'
                            }
                        }
                        await MainController.sendNotif(notifData)

                        ibxSucc.push(inbox.ibx_refid);
                    }else{
                        SendErrorController.message(`Failed to Create Cashflow Certificate}`)
                    }
                }
                resolve(ibxSucc)
            }catch(err){
                console.log(err);
                SendErrorController.message(`Error on Cron Buy Certificate\n${err.stack}`)
                resolve(false);
            }
        }
    })
}

function startCron () {
    let timeout;
    Promise.all([MainController.commandJobs()])
    .then(processing)
    .then(response => {
        clearTimeout(timeout)
        timeout = setTimeout(startCron, 750)
    })
}
setTimeout(() => startCron());