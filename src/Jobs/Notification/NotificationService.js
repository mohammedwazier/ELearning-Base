
require('module-alias/register')

const { STRUCTURE, WHATSAPP } = require('@Config/Config');
const database = require('@Model/index');
let MainController = require('@Controllers/MainController');
MainController = new MainController();
const SendErrorController = require('@Controllers/SendErrorController');
const moment = require('moment-timezone')
moment.tz('Asia/Jakarta')

const { accountSid, authToken } = WHATSAPP;
const client = require('twilio')(accountSid, authToken);

const processing = async () => {
    return new Promise(async (resolve) => {
        let data = await database.produk.allSelect({produk_announce: 0});
        for(let idx = 0; idx < data.length; idx++){
            const d = data[idx];
            try{
                let timeDatabase = d.produk_startjam.split(':');
                let timeNow = moment().format();
                let dateDatabase = moment(d.produk_start);
                dateDatabase.set({
                    hours: timeDatabase[0],
                    minute: timeDatabase[1],
                    second: timeDatabase[2]
                })
                dateDatabase = moment(dateDatabase).format()
                const diff = moment().diff(dateDatabase, 'minutes') * -1;

                if(Number(diff) <= 30){
                    // console.log('masuk sini');
                    /* let produkEnd = prdStart
                    produkEnd = produkEnd.setMinutes(produkEnd.getMinutes() - 30);
                    produkEnd = new Date(produkEnd);
                    let timeA = produkEnd.getTime()
                    let timeB = nowDate.getTime();
    
                    if(timeB > timeA){ */
                        // Send Email
                        // Send Notif
                        // Dalam 30 Menit lagi, Produk [nama_produk] akan segera dibuka, mohon berisap
                        // Ambil data User yang membeli produk ini
                        // let userQuery = await database.profile.connection.raw(`SELECT * FROM profile JOIN`)
    
                    let User = await database.transaksi.allSelect({
                        trx_produk_id: d.produk_id
                    });
                    

                    for(let ida = 0; ida < User.length; ida++){
                        let ProfileIdUser = User[ida];
                        let profileAkun = await database.profile.single({prl_profile_id: ProfileIdUser.trx_id_profile});
                        let notifData = {
                            data: {
                                id: profileAkun.prl_profile_id,
                                title: 'Notifikasi',
                                message: `Seminar ${d.produk_namaProduk}.
akan dimulai dalam ${diff} Menit.`,
                                nama_sender: 'Prexux',
                                menu: 'trx',
                                tipe: 'default',

                                send: 'user'
                            }
                        }
                        // console.log(notifData)
                        await MainController.sendNotif(notifData)
                    }

                    await database.produk.updateOne({id: d.id}, {produk_announce: 1});
                    continue;
                }else if(diff < 0){
                    await database.produk.updateOne({id: d.id}, {produk_announce: 1});
                    continue;
                }else{
                    continue;
                }
            }catch(err){
                console.log(err)
                SendErrorController.message(`Error on NotificationService\n${err.stack}`)
                continue;
            }
        }
        resolve(true);
    })
}

function startCron () {
    let timeout;
    Promise.all([MainController.commandJobs()])
    .then(processing)
    .then(response => {
        clearTimeout(timeout)
        timeout = setTimeout(startCron, (1000 * 10) /*60 Seconds*/)
    })
}
setTimeout(() => startCron());