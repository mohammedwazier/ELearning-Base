// import Database from '../Database/Database';
const Database = require('../Model/index');
const {
    NODE_ENV
} = require('../Config/Config');
const {
    v4: uuidv4
} = require('uuid');

const LoggingAccess = async (req, res, next) => {
    try {
        if (req.headers['authorization'] || req.headers.authorization) {
            let token = req.headers['authorization'] || req.headers.authorization;
            token = token.split(' ');
            token = token[token.length - 1];
            const used = (process.memoryUsage().heapUsed / 1024 / 1024);
            const round = Math.round(used * 100) / 100;
            await Database.access_api.insertOne({
                acc_ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                acc_url: req.originalUrl,
                acc_node: NODE_ENV,
                acc_token: token,
                acc_uid: uuidv4(),
                acc_data: {
                    ...req.body,
                    ...req.params,
                    ...req.query
                },
                acc_memory: round
            })
        } else {
            const used = (process.memoryUsage().heapUsed / 1024 / 1024);
            const round = Math.round(used * 100) / 100;
            await Database.access_api.insertOne({
                acc_ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                acc_url: req.originalUrl,
                acc_node: NODE_ENV,
                acc_uid: uuidv4(),
                acc_data: {
                    ...req.body,
                    ...req.params,
                    ...req.query
                },
                acc_memory: round
            })
        }
        next();
    } catch (err) {
        console.log(err)
        return res.status(502).send({
            auth: false,
            state: false,
            code: 502,
            message: 'Failed to authenticate token'
        });
    }
}

module.exports = LoggingAccess;