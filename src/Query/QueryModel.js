const profileSelect = (profileData) => {
    return `
    SELECT * from 
    profile 
    WHERE
    prl_nik = '${profileData.prl_nik}'
    OR
    prl_nama = '${profileData.prl_nama}'
    OR
    prl_nohp = '${profileData.prl_nohp}'
    OR
    prl_username = '${profileData.prl_username}'
    `
}

const kodeOtpSelect = (kode, number, today) => {
    return `
    SELECT * FROM 
    public.otp_list 
    WHERE 
    otp_kode LIKE '%${kode}%' AND otp_nohp LIKE '%${number}%' AND otp_created_at LIKE '${today}%' AND otp_status = 0`
}

// exports.module = {
//     profileSelect,
//     kodeOtpSelect
// }

module.exports = {
    profileSelect,
    kodeOtpSelect
}