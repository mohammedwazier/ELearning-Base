const ProfileController = require('@Controllers/ProfileController')
const VerifyMiddleware = require('@Middleware/VerifyMiddleware')
const Busboy = require('connect-busboy')

module.exports = (router) => {
    router.use(Busboy({
        highWaterMark: 50 * 1024 * 1024, // Set 50MiB buffer
    })) // Insert the busboy middle-ware

    router.post('/getProfile', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.getDetailProfile(['id'], req.body)
        return res.send(response)
    })

    router.post('/updateProfile', VerifyMiddleware, async (req, res) => {
        req.body.token = req.headers.authorization.split(' ');
        req.body.token = req.body.token[req.body.token.length - 1];
        // let response = await ProfileController.updateProfile(['nama', 'tanggal_lahir', 'tempat_lahir', 'gender', 'username', 'gelar', 'gelar_profesi', 'password'], req.body)
        let response = await ProfileController.updateProfile(['id'], req.body)
        return res.send(response)
    })

    router.post('/updatePhoto', VerifyMiddleware, async (req, res) => {
        let uploadImage = await ProfileController.uploadImage(req)
        if(Boolean(uploadImage.state) === true){
            console.log(uploadImage);
            data = uploadImage.data.fieldData;
            let response = await ProfileController.updatePhoto(['id', 'image'], data)
            return res.send(response)
        }else{
            return res.status(500).send({state: false, message: "Failed to Upload Image", code: 105})
        }
    })

    router.post('/checkUsername', async (req, res) => {
        let response = await ProfileController.checkUsername(['username'], req.body)
        return res.send(response)
    })

    router.post('/getKotaIndonesia', async (req, res) => {
        let response = ProfileController.getKota()
        return res.send(response)
    })

    router.get('/getPhoto/:id', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.getProfilePicture(['id'], req.params)
        return res.send(response)
    })

    router.post('/changePassword', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.changePassword(['id', 'password'], req.body);
        return res.send(true);
    })

    router.post('/verityOTPchangePassword', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.verifyOTPChangePassword(['id', 'otp'], req.body);
        return res.send(true);
    })

    router.post('/requestChangePrimaryData', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.requestChangePrimaryData(['id', 'type'], req.body);
        return res.send(response);
    })

    router.post('/changePrimaryData' ,VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.changePrimaryData(['id', 'type', 'value'], req.body);
        return res.send(response)
    })

    // Dashboard
    router.post('/all', VerifyMiddleware, async(req, res) => {
        req.body = Object.assign({
            ...req.body,
            ...req.query
        })
        let response = await ProfileController.getAll(req.body)
        return res.send(response)
    })

    router.post('/updateEmail', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.updateEmail(['id', 'new_email'], req.body);
        return res.send(response);
    })

    router.post('/updatePhoneNumber', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.updatePhoneNumber(['id', 'new_nohp'], req.body);
        return res.send(response);
    })

    router.post('/ConfirmOTP', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.confirmOtp(['id', 'otp', 'type', 'changedvalue'], req.body);
        return res.send(response);
    })

    // router.post('/allPemateri', VerifyMiddleware, async (req, res) => {
    //     let response = await ProfileController.getAllPemateri()
    //     res.send(response)
    // })

    router.post('/detail', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.detailUser(['id', 'profileid'], req.body)
        return res.send(response)
    })

    router.post('/searchProfile', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.search(['id', 'search'], req.body);
        return res.send(response)
    })

    router.post('/suspendUser', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.suspend(['id', 'profileid'], req.body);
        return res.send(response);
    })

    router.post('/deleteUser', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.delete(['id', 'profileid'], req.body);
        return res.send(response);
    })

    router.post('/selectRoleUser', VerifyMiddleware, async (req, res) => {
        let response = await ProfileController.selectRole(['id', 'role'], req.body);
        return res.send(response)
    })
    return router
}