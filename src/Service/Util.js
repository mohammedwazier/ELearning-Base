const UtilizationController = require('@Controllers/UtilizationController.js');
const VerifyMiddleware = require('@Middleware/VerifyMiddleware')

const fs = require('fs');
const Busboy = require('connect-busboy');

module.exports = (router) => {

    router.use(Busboy({
        highWaterMark: 50 * 1024 * 1024, // Set 50MiB buffer
    })); // Insert the busboy middle-ware

    router.get('/getUtil', async (req, res) => {
    	let response = await UtilizationController.getNumber();
    	return res.send(response).status(200);
    })

    router.get('/validatePassword/:username/:password/:group', async (req, res) => {
    	let response = await UtilizationController.validatePassword(req.params)
    	return res.send(response);
    })

    router.post('/GetSetting', VerifyMiddleware, async (req, res) => {
    	let response = await UtilizationController.listSetting(['id'], req.body);
    	return res.send(response);
    })

    router.post('/CreateSetting', VerifyMiddleware, async (req, res) => {
    	let response = await UtilizationController.createSetting(['kode', 'typevalue', 'value', 'keterangan', 'id', 'active'], req.body);
    	return res.send(response)
    })

    router.post('/SingleSetting', VerifyMiddleware, async (req, res) => {
    	let response = await UtilizationController.singleSetting(['id', 'kode'], req.body);
    	return res.send(response);
    })

    router.post('/UpdateSetting', VerifyMiddleware, async (req, res) => {
    	let response = await UtilizationController.updateSetting(['idsetting', 'kode', 'typevalue', 'value', 'keterangan', 'id', 'active'], req.body);
    	return res.send(response);
    })

    router.post('/HomepageStatistic', VerifyMiddleware, async (req, res) => {
    	let response = await UtilizationController.Homepage(['id'], req.body);
    	return res.send(response)
    })

    /*Privacy Policy*/
    router.get('/PrivacyPolicy', async (req, res) => {
        let response = await UtilizationController.PrivacyPolicy();
        return res.send(response);
    })

    // router.get('/PrivacyPolicy/html', async (req, res) => {
        
    // })

    router.post('/AddUserguide', VerifyMiddleware, async (req, res) => {
        let uploadFile = await UtilizationController.uploadImage(req);
        if(Boolean(uploadFile.state) === true){
            const data = uploadFile.data.fieldData;
            let response = await UtilizationController.CreateUserguide(['id', 'file'], data);
            return res.send(response)
        }else{
            return res.status(500).send({state: false, message: "Failed to Upload File", code: 105})
        }
    })

    router.post('/UserguideList', async (req, res) => {
        let response = await UtilizationController.getListUserGuide();
        return res.send(response);
    })

    router.post('/DeleteUserguide', VerifyMiddleware, async (req, res) => {
        let response = await UtilizationController.DeleteUserguide(['id', 'guideid'], req.body);
        return res.send(response);
    })

    router.get('/userguide/:name', (req, res) => {
        const { name } = req.params;
        let stream = fs.readStream(`../Source/${name}`);

        res.setHeader('Content-disposition', 'inline; filename="' + name + '"');
        res.setHeader('Content-type', 'application/pdf');

        stream.pipe(res);
    })

    return router;
}