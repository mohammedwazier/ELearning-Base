const VerifyMiddleware = require('@Middleware/VerifyMiddleware');
const CashflowController = require('@Controllers/CashflowController');
// const Busboy = require('connect-busboy');

module.exports = (router) => {
	router.post('/allCashflow', VerifyMiddleware, async (req, res) => {
        let response = await CashflowController.getAllCashflow(['id'], req.body);
        return res.send(response);
	})

	router.post('/getSingleCashflow', VerifyMiddleware, async (req, res) => {
		let response = await CashflowController.getSingleCashflow(['id'], req.body);
		return res.send(response);
	})

	/*Dashboard*/
	router.post('/singleCashflow', VerifyMiddleware, async (req, res) => {
		let response = await CashflowController.singleCashflow(['id','refid'], req.body);
		return res.send(response);
	})
	return router;
};