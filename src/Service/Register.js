// import { Request, Response } from 'express';
const RegisterController = require('@Controllers/RegisterController');
const VerifyMiddleware = require('@Middleware/VerifyMiddleware');

module.exports = (router) => {
    router.post('/Register', async (req, res) => {
        let validasiRegister = await RegisterController.registerUser(['nohp', 'email', 'nama', 'username', 'password', 'tipe', 'otp', 'group'], req.body);
        return res.send(validasiRegister);
    })

    router.post('/Register/DashboardAdmin', VerifyMiddleware, async (req, res) => {
        let validasiRegister = await RegisterController.registerUserDashboard(['nohp', 'email', 'nama', 'username', 'password', 'tipe'], req.body);
        return res.send(validasiRegister);
    })

    router.post('/validasi', async (req, res) => {
        let validasi = await RegisterController.validasi(['nohp', 'username', 'email', 'otp', 'group'], req.body);
        return res.send(validasi);
    })

    router.post('/whatsappRegister', async (req, res) => {
        // let registerWhatsapp = await RegisterController.registerWhatsapp(req.body, res);
        let response = await RegisterController.registerWhatsapp(['sender', 'message', 'id', 'created'], req.body);
        return res.send(response);
    })

    router.post('/forgotPassword', async (req, res) => {
        let response = await RegisterController.forgotPassword(['value'], req.body);
        return res.send(true);
    })

    router.post('/validateForgotPassword', async (req, res) => {
        let response = await RegisterController.validateForgotPassword(['otp', 'value'], req.body);
        return res.send(true);
    })

    router.get('/testingsendWhatsapp', async (req, res) => {
        let response = await RegisterController.testSendWa();
        return res.send(response);
    })

    // router.get('/', async (req, res) => {
    //     res.send(false);
    // })
    return router;
}