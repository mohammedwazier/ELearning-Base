const VerifyMiddleware = require('@Middleware/VerifyMiddleware');
const EventController = require('@Controllers/EventController');
const Busboy = require('connect-busboy');

module.exports = (router) => {
	router.use(Busboy({
        highWaterMark: 50 * 1024 * 1024, // Set 50MiB buffer
    })); // Insert the busboy middle-ware

	/*Dashboard*/
	router.post('/allEvent', VerifyMiddleware, async (req, res) => {
		req.body = {
			...req.body,
			...req.query
		}
		let response = await EventController.getAllEvent(['id'], req.body);
		return res.send(response);
	})

	router.post('/searchEvent', VerifyMiddleware, async (req, res) => {
		req.body = {
			...req.body,
			...req.query
		}
		let response = await EventController.searchEvent(['id', 'search'], req.body);
		return res.send(response);
	})

	router.post('/createEvent', VerifyMiddleware, async (req, res) => {
		let uploadImage = await EventController.uploadImage(req)
		if(Boolean(uploadImage.state) === true){
			let data = uploadImage.data.fieldData;
			let response = await EventController.createEvent(['id', 'name', 'description', 'method', 'startdate', 'enddate', 'starthour', 'endhour', 'nilai', 'used'], data);
			return res.send(response);
		}else{
			return res.status(500).send({state: false, message: "Failed to Upload Image", code: 105})
		}
	})

	router.post('/updateEvent', VerifyMiddleware, async (req, res) => {
		let uploadImage = await EventController.uploadImage(req)
		if(Boolean(uploadImage.state) === true){
			let data = uploadImage.data.fieldData;
			let response = await EventController.updateEvent(['id', 'name', 'description', 'method', 'startdate', 'enddate', 'starthour', 'endhour', 'nilai', 'used', 'eventid'], data);
			return res.send(response);
		}else{
			return res.status(500).send({state: false, message: "Failed to Upload Image", code: 105})
		}
	})

	router.post('/singleEvent', VerifyMiddleware, async (req, res) => {
		let response = await EventController.single(['id', 'event_id'], req.body);
		return res.send(response);
	})

	router.post('/softDelete', VerifyMiddleware, async (req, res) => {
		let response = await EventController.softDelete(['id', 'eventid'], req.body);
		return res.send(response);
	})
	return router;
};